package card;

import gameServer.Player;
import gameServer.UnitCard;

public class TenaciousMarines extends UnitCard {
	
	public TenaciousMarines(Player player){
		super(player);
	}
	
	@Override
	public void setup (){
		this.attrs.set("cost", 50);
		this.attrs.set("attack", 5);
		this.attrs.set("health", 4);
		attrs.setTrue("tenacity");
		this.name = "Tenacious Marines";
		this.unitType = UnitType.FIGHTER;
		this.description = "Tenacity: this unit takes half damage on defense, rounded up";
		//this.activeAbilities.add(new TargetChangeCooldown(10, this, 1, 1));
	}
	
	
}

package card;

import gameServer.Player;
import gameServer.UnitCard;

public class CombatDrone extends UnitCard {

	public CombatDrone(Player player) {
		super(player);
	}

	@Override
	public void setup() {
		super.setup();
		attrs.set("cost", 20);
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.setTrue("mobility");
		this.unitType = UnitType.FIGHTER;
		name = "Combat Drone";
		description = "Makeshift Repairs: this unit loses an additional cooldown per turn";
		//passiveAbilities = new ArrayList<PassiveAbility>();
		//passiveAbilities.add(new ChangeSelfCooldown(this, 1));
	}

}

package card;

import gameServer.Player;
import gameServer.UnitCard;

public class SwashbucklingMatey extends UnitCard {

	public SwashbucklingMatey(Player player) {
		super(player);
	}

	@Override
	public void setup() {
		super.setup();
		attrs.set("cost", 20);
		attrs.set("attack", 3);
		attrs.set("health", 4);
		name = "Swashbuckling Matey";
		this.unitType = UnitType.FIGHTER;
		description = "Extortion: 2<br/>When deployed, gain debt 30";
		attrs.set("extortion", 2);
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 30;
		owner.dirty = true;
	}
	
	

}

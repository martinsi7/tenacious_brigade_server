package card;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetChangeCooldown;

public class FieldMedic extends UnitCard {

	public FieldMedic(Player player) {
		super(player);
	}

	@Override
	public void setup() {
		super.setup();
		attrs.set("cost", 30);
		attrs.set("attack", 1);
		attrs.set("health", 4);
		name = "Field Medic";
		this.unitType = UnitType.SPECIALIST;
		activeAbilities = new ArrayList<ActiveAbility>();
		SameLaneTargetter targetter = new SameLaneTargetter();
		activeAbilities.add(new TargetChangeCooldown(targetter, 20, this, -2, 2, false));
	}

}

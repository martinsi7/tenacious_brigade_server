package card;

import java.util.ArrayList;

import abilityTargetter.OppositeLaneTargetter;
import activeAbilities.TargetChangeCooldown;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

public class Assassin extends UnitCard {

	public Assassin(Player player) {
		super(player);
	}

	@Override
	public void setup() {
		super.setup();
		this.attrs.set("cost", 40);
		this.attrs.set("attack", 1);
		this.attrs.set("health", 1);
		this.name = "Assassin";
		this.description = "";
		this.unitType = UnitType.SPECIALIST;
		this.activeAbilities = new ArrayList<ActiveAbility>();
		OppositeLaneTargetter targetter = new OppositeLaneTargetter();
		this.activeAbilities.add(new TargetChangeCooldown(targetter, 20, this, 2, 1, true));
	}

}

package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class ReinforcedTurret extends UnitCard {

	public ReinforcedTurret(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Reinforced Turret";
		description = "Defensive";
		attrs.set("attack", 3);
		attrs.set("health", 4);
		attrs.set("cost", 20);
		attrs.setTrue("defensive");
		unitType = UnitType.FIGHTER;
	}

}

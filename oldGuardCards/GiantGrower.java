package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetMultipleAbilities;

public class GiantGrower extends UnitCard  {
	public boolean hasBuff = false;

	public GiantGrower(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Giant Grower";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		String[] attrs = {"attack", "health"};
		int[] amounts = {2, 2};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(0, this, 1, targetter, true, attrs, amounts, false, true);
		ability.description = "$0, cooldown: target friendly unit in this lane gets +2/+2 " +
				"until end of turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}
	
}

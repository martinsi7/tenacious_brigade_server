package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class TurretedBarricade extends UnitCard {

	public TurretedBarricade(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Turreted Barricade";
		description = "Reach, Defensive";
		attrs.set("attack", 1);
		attrs.set("health", 5);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("defensive");
		attrs.setTrue("reach");
	}

}

package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.MechCardInfo;
import gameServer.Player;

import java.util.ArrayList;

import abilityTargetter.AllOwnedTargetter;
import activeAbilities.TargetMultipleAbilities;

public class MightOfExplosives extends MechCardInfo {

	public MightOfExplosives(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		name = "Might of Explosives";
		description = "";
		attrs.set("attack", 1);
		attrs.set("cost", 30);
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AllOwnedTargetter targetter = new AllOwnedTargetter(Faction.OLD_GUARD);
		String[] attrs = {"attack", "health"};
		int[] amounts = {3, 3};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(50, this, 2, targetter, false, attrs, amounts, false, true);
		
		ability.description = "$50: give 2 target Old Guard units +3/+3 unit end of turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}
}

package oldGuardCards;

import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class HelmOfCooperation extends MechCardInfo {

	public HelmOfCooperation(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		name = "Helm of Cooperation";
		description = "At the beginning of your turn," +
				" All friendly Old Guard fighters gain +1/+1 for each " +
				"Old Guard specialist they share a lane with until the " +
				"beginning of your next turn";
		attrs.set("cost", 30);
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		int 	numLeftSpecialists = countSpecialists(GameState.Zone.LEFT),
				numCenterSpecialists = countSpecialists(GameState.Zone.CENTER),
				numRightSpecialists = countSpecialists(GameState.Zone.RIGHT);
		for (UnitCard unit : owner.getUnitsInEventOrder()) {
			if (unit.unitType == UnitType.FIGHTER && !unit.isCooldown) {
				int amount = 0;
				switch (unit.zone) {
				case LEFT:
					amount = numLeftSpecialists;
					break;
				case RIGHT:
					amount = numRightSpecialists;
					break;
				case CENTER:
					amount = numCenterSpecialists;
					break;
				default:
					break;
				}
				unit.attrs.addChanger("health", this, true, false, amount);
				unit.attrs.addChanger("attack", this, true, false, amount);
			}
				
		}
		
	}
	
	private int countSpecialists(GameState.Zone _zone) {
		int count = 0;
		for (CardInfo card : owner.getZoneCards(_zone)) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.faction == Faction.OLD_GUARD &&
						unit.unitType == UnitCard.UnitType.SPECIALIST) {
					count++;
				}
			}
		}
		return count;
	}

}

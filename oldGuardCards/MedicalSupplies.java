package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.MechCardInfo;
import gameServer.Player;

import java.util.ArrayList;

import abilityTargetter.AllOwnedFighterCooldown;
import activeAbilities.TargetChangeCooldown;

public class MedicalSupplies extends MechCardInfo {

	public MedicalSupplies(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		name = "Medical Supplies";
		description = "";
		attrs.set("attack", 0);
		attrs.set("cost", 40);
		setActiveAbilites();
	}
	
	private void setActiveAbilites() {
		AllOwnedFighterCooldown targetter = new AllOwnedFighterCooldown();
		TargetChangeCooldown ability = new TargetChangeCooldown(targetter, 10, this, -1, 1, false);
		ability.description = "$10: reduce target friendly fighter's cooldown by one";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetMultipleAbilities;

public class BehemothGrower extends UnitCard {

	public BehemothGrower(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Behemoth Grower";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		String[] attrs = {"attack", "health"};
		int[] amounts = {1, 3};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(0, this, 1, targetter, true, attrs, amounts, true, false);
		ability.description = "$0, cooldown: target friendly unit in this lane gets +1/+3. " +
				"until the beginning of your next turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

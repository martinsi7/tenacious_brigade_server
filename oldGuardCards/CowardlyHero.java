package oldGuardCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class CowardlyHero extends UnitCard {

	public CowardlyHero(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Cowardly Hero";
		description = "Deployment: If there is a unit with 5 " +
				"or more attack in this lane, gain +2/+2";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 20);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.zone == zone && unit.owner == owner
						&& unit.attrs.get("attack") >= 5) {
					attrs.change("attack", 2);
					attrs.change("health", 2);
					break;
				}
			}
		}
	}

}

package oldGuardCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class MilitaryTransport extends UnitCard {

	public MilitaryTransport(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Military Transport";
		description = "At the beginning of your turn, each " +
				"friendly fighter in this lane gains " +
				"Mobility until end of turn";
		attrs.set("attack", 2);
		attrs.set("health", 3);
		attrs.set("cost", 60);
		unitType = UnitType.SPECIALIST;
	}

	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.isCooldown) {
					continue;
				}
				unit.attrs.addChanger("mobility", this, true, false);
			}
		}
	}

}

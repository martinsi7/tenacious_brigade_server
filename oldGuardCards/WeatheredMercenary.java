package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class WeatheredMercenary extends UnitCard {

	public WeatheredMercenary(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 10;
		owner.dirty = true;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Weathered Mercenary";
		description = "Tenacity, Contract: 10";
		attrs.set("attack", 5);
		attrs.set("health", 3);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("tenacity");
	}

}

package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class ShortFusedDroid extends UnitCard {

	public ShortFusedDroid(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Short Fused Droid";
		description = "Shambling<br/>If this unit takes combat damage, it gains +1/+0";
		attrs.set("attack", 4);
		attrs.set("health", 6);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
		attrs.set("shambling", true);
	}
	
	@Override
	public void takeDamage(int amount) {
		if (amount > 0) {
			attrs.change("attack", 1);
		}
		super.takeDamage(amount);
	}

}

package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetChangeAbilities;

public class StimulantVendor extends UnitCard {

	public StimulantVendor(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Stimulant Vendor";
		description = "";
		attrs.set("attack", 0);
		attrs.set("health", 4);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		TargetChangeAbilities ability = new TargetChangeAbilities(10, this, 1, targetter, "tenacity", false, true, false);
		ability.description = "$10: target unit in this lane gains tenacity until the end of your next turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package oldGuardCards;

import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

public class DistractingBattlecry extends MechCardInfo{

	public DistractingBattlecry(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}
	
	@Override 
	public void isDeployed() {
		ArrayList<UnitCard> leftUnits = new ArrayList<UnitCard>();
		ArrayList<UnitCard> centerUnits = new ArrayList<UnitCard>();
		ArrayList<UnitCard> rightUnits = new ArrayList<UnitCard>();
		super.isDeployed();
		for (CardInfo card : GameState.state.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner == owner) {
					if (unit.zone == GameState.Zone.LEFT) {
						leftUnits.add(unit);
					} else if (unit.zone == GameState.Zone.RIGHT) {
						rightUnits.add(unit);
					} else if (unit.zone == GameState.Zone.CENTER) {
						centerUnits.add(unit);
					}
				}
			}
		}
		
		for(UnitCard unit : leftUnits) {
			unit.attrs.addChanger("health", this, false, false, leftUnits.size() - 1);
			unit.dirty = true;
		}
		for(UnitCard unit : centerUnits) {
			unit.attrs.addChanger("health", this, false, false, centerUnits.size() - 1);
			unit.dirty = true;
		}
		for(UnitCard unit : rightUnits) {
			unit.attrs.addChanger("health", this, false, false, rightUnits.size() - 1);
			unit.dirty = true;
		}
	}

	@Override
	public void setup() {
		name = "Distracting Battlecry";
		description = "Deployment: All friendly Old Guard units get +0/+1 " +
				"for each Old Guard unit it shares a lane with";
		attrs.set("attack", 1);
		attrs.set("cost", 40);
	}

}

package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class VigilantSentry extends UnitCard {

	public VigilantSentry(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Vigilant Sentry";
		description = "Defensive";
		attrs.set("attack", 4);
		attrs.set("health", 4);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("defensive");
	}

}

package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SelfTargetter;
import activeAbilities.TargetMultipleAbilities;

public class RangedArtilleryExpert extends UnitCard {

	public RangedArtilleryExpert(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Ranged Artillery Expert";
		description = "";
		attrs.set("attack", 4);
		attrs.set("health", 4);
		attrs.set("cost", 50);
		unitType = UnitType.FIGHTER;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SelfTargetter targetter = new SelfTargetter();
		String[] attrs = {"attack", "health"};
		int[] amounts = {1, 1};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(10, this, 1, targetter, false, attrs, amounts, true, false);
		ability.description = "$10: this unit gets +1/+1 until beginning of your next turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

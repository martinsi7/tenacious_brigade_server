package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class AutomatedDefenseSystem extends UnitCard {

	public AutomatedDefenseSystem(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Automated Defense System";
		description = "Reach, Tenacity, Defensive";
		attrs.set("attack", 4);
		attrs.set("health", 4);
		attrs.set("cost", 80);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("reach");;
		attrs.setTrue("defensive");
		attrs.setTrue("tenacity");
	}

}

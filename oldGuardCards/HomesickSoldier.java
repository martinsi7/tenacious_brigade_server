package oldGuardCards;

import gameServer.GameState;
import gameServer.Player;
import gameServer.UnitCard;

public class HomesickSoldier extends UnitCard {

	public HomesickSoldier(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Homesick Soldier";
		description = "Shambling, Deployment: If a " +
				"building is active, gain +5/+2";
		attrs.set("attack", 1);
		attrs.set("health", 4);
		attrs.set("cost", 40);
		attrs.setTrue("shambling");
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		GameState state = owner.gameState;
		if (state.curPlayer.deck.building.isDeployed ||
				state.otherPlayer.deck.building.isDeployed) {
			attrs.change("attack", 5);
			attrs.change("health", 2);
		}
	}

}

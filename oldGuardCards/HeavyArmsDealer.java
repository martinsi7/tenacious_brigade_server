package oldGuardCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class HeavyArmsDealer extends UnitCard {

	public HeavyArmsDealer(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Heavy Arms Dealer";
		description = "At the beginning of your turn, the fighter with the highest health" +
				" in this lane gets +3/+3 until the beginning of your next turn.";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 40);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		int highestHealth = 0;
		UnitCard target = null;
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit != null && !unit.isCooldown && unit.unitType == UnitType.FIGHTER) {
				if (unit.getAttr("health") > highestHealth) {
					target = unit;
					highestHealth = unit.getAttr("health");
				}
			}
		}
		if (target != null) {
			target.attrs.addChanger("attack", this, true, false, 3);
			target.attrs.addChanger("health", this, true, false, 3);
		}
	}

}

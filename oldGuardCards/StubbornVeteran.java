package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SelfTargetter;
import activeAbilities.TargetChangeAbilities;

public class StubbornVeteran extends UnitCard {

	public StubbornVeteran(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Stubborn Veteran";
		description = "Defensive, Regeneration";
		attrs.set("attack", 1);
		attrs.set("health", 4);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("regeneration");
		attrs.setTrue("defensive");
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SelfTargetter targetter = new SelfTargetter();
		TargetChangeAbilities ability = new TargetChangeAbilities(10, this, 1, targetter, "attack", 2, false, true, false);
		ability.description = "$10: this unit gets +2/+0 until the beginning of your next turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}
	
}

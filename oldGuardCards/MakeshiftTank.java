package oldGuardCards;

import gameServer.Player;
import gameServer.UnitCard;

public class MakeshiftTank extends UnitCard {

	public MakeshiftTank(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Makeshift Tank";
		description = "Defensive, Shambling<br/>If this unit takes" +
				"combat damage, it loses defensive";
		attrs.set("attack", 5);
		attrs.set("health", 8);
		attrs.set("cost", 50);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("defensive");
		attrs.setTrue("shambling");
	}
	
	@Override
	public void takeDamage(int amount) {
		attrs.setFalse("defensive");
		super.takeDamage(amount);
	}

}

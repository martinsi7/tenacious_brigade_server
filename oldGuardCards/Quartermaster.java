package oldGuardCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetAllLaneAbilities;

public class Quartermaster extends UnitCard {

	public Quartermaster(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Quartermaster";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		String[] attrs = {"attack", "health"};
		int[] amounts = {1, 2};
		TargetAllLaneAbilities ability = new TargetAllLaneAbilities(0, this, 1, targetter, true, attrs, amounts, true, false);
		ability.description = "$0, cooldown: all units in this lane get +1/+2 until " +
				"the beginning of your next turn. ";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package oldGuardCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class SupportMarine extends UnitCard {

	public SupportMarine(Player player) {
		super(player);
		faction = Faction.OLD_GUARD;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Support Marine";
		description = "Deployment: Gain +1/+1 for each friendly" +
				"specialist in the lane with it";
		attrs.set("attack", 4);
		attrs.set("health", 3);
		attrs.set("cost", 50);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.zone == zone && unit.owner == owner
						&& unit.unitType == UnitType.SPECIALIST) {
					attrs.change("attack", 1);
					attrs.change("health", 1);
				}
			}
		}
	}

}

package mechCard;

import gameServer.MechCardInfo;
import gameServer.Player;

public class ForceHammer extends MechCardInfo {

	public ForceHammer(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Force Hammer";
		description = "";
		attrs.set("attack", 1);
		attrs.set("cost", 20);
	}

}

package mechCard;

import gameServer.MechCardInfo;
import gameServer.Player;

public class NuclearArmament extends MechCardInfo {

	public NuclearArmament(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Nuclear Armament";
		description = "";
		attrs.set("attack", 4);
		attrs.set("cost", 100);
	}

}

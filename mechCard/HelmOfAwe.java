package mechCard;

import gameServer.MechCardInfo;
import gameServer.Player;

public class HelmOfAwe extends MechCardInfo {
	
	public HelmOfAwe(Player player) {
		super(player);
		attrs.set("attack", 1);
		attrs.set("cost", 60);
		//passiveAbilities = new ArrayList<PassiveAbility>();
		//passiveAbilities.add(new ChangeCooldownMine(this, -1));
		name = "Helm of Awe";
		description = "at the beginning of each turn, reduce all friendly unit's cooldown by one.";
		
		
	}

	@Override
	public void setup() {
	}
	
	
	
	

}

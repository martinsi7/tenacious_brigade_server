package mechCard;

import gameServer.ActiveAbility;
import gameServer.MechCardInfo;
import gameServer.Player;

import java.util.ArrayList;

import abilityTargetter.AllOwnedTargetter;
import activeAbilities.TargetMultipleAbilities;

public class CrackedHelmet extends MechCardInfo {

	public CrackedHelmet(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Cracked Helmet";
		description = "";
		attrs.set("attack", 0);
		attrs.set("cost", 40);
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AllOwnedTargetter targetter = new AllOwnedTargetter(Faction.NEUTRAL);
		String[] attrs = {"attack", "health"};
		int[] amounts = {2, 2};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(20, this, 1, targetter, false, attrs, amounts, false, true);
		ability.description = "20: Give target friendly unit +1/+1 until end of turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

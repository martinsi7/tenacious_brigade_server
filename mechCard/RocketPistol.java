package mechCard;

import gameServer.MechCardInfo;
import gameServer.Player;

public class RocketPistol extends MechCardInfo {

	public RocketPistol(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Rocket Pistol";
		description = "";
		attrs.set("attack", 2);
		attrs.set("cost", 30);
	}

}

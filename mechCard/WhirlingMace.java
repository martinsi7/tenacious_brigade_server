package mechCard;

import gameServer.MechCardInfo;
import gameServer.Player;

public class WhirlingMace extends MechCardInfo {

	public WhirlingMace(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Whirling Mace";
		description = "";
		attrs.set("attack", 3);
		attrs.set("cost", 50);
	}

}

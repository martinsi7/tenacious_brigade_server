package buildings;

import gameServer.BuildingCard;
import gameServer.Player;

public class HiddenVault extends BuildingCard {

	public HiddenVault(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Hidden Vault";
		description = "Deployment: Each player's maximum Credits becomes 150";
		attrs.set("cost", 100);
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.maxCredits = 150;
		owner.gameState.otherPlayer.maxCredits = 150;
	}

}

package buildings;

import gameServer.BuildingCard;
import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class OpenArmory extends BuildingCard {

	public OpenArmory(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Open Armory";
		description = "Deployment: All Specialists gains +3/+3";
		attrs.set("cost", 100);
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			UnitCard unit = UnitCard.asUnit(card);
			if (unit != null && unit.unitType == UnitType.SPECIALIST) {
				unit.attrs.addChanger("attack", this, false, false, 3);
				unit.attrs.addChanger("health", this, false, false, 3);
			}
		}
	}

}

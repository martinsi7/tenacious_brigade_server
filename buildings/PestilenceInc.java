package buildings;

import gameServer.BuildingCard;
import gameServer.Player;
import gameServer.UnitCard;

public class PestilenceInc extends BuildingCard {

	public PestilenceInc(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Pestilence, Inc.";
		description = "At the beginning of each player's turn, " +
				"that player loses 1 health, " +
				"and all that player's allied units gain -1/-1";
		attrs.set("cost", 100);
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		owner.health -= 1;
		owner.dirty = true;
		for (UnitCard unit : owner.getUnitsInEventOrder()) {
			unit.attrs.addChanger("attack", this, false, false, 1);
			unit.attrs.addChanger("health", this, false, false, 1);
			if (unit.isDead()) {
				unit.goToClinic();
			}
		}
	}
	
	@Override
	public void endTurnAction() {
		super.endTurnAction();
		Player player = owner.gameState.otherPlayer;
		player.health -= 1;
		player.dirty = true;
		for (UnitCard unit : player.getUnitsInEventOrder()) {
			unit.attrs.addChanger("attack", this, false, false, 1);
			unit.attrs.addChanger("health", this, false, false, 1);
			if (unit.isDead()) {
				unit.goToClinic();
			}
		}
	}

}

package buildings;

import gameServer.BuildingCard;
import gameServer.Player;
import gameServer.UnitCard;

public class FieldClinic extends BuildingCard {

	public FieldClinic(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Field Clinic";
		description = "At the beginning of each turn, all cooldown on all units is reduced to 0";
		attrs.set("cost", 100);
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (UnitCard unit : owner.getUnitsInEventOrder()) {
			if (unit.isCooldown) {
				unit.cooldown = 0;
				unit.dirty = true;
			}
		}
	}
	
	@Override
	public void endTurnAction() {
		super.endTurnAction();
		for (UnitCard unit : owner.gameState.otherPlayer.getUnitsInEventOrder()) {
			if (unit.isCooldown) {
				unit.cooldown = 0;
				unit.dirty = true;
			}
		}
	}

}

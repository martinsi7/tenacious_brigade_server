package buildings;

import gameServer.BuildingCard;
import gameServer.GameState;
import gameServer.Player;
import gameServer.UnitCard;

public class FlankingSupportTowers extends BuildingCard {

	public FlankingSupportTowers(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}

	@Override
	public void setup() {
		name = "Flanking Support Towers";
		description = "At the beginning of each player's next turn, " +
				"All units in the left and right lanes gain aggressive ";
		attrs.set("cost", 100);
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (UnitCard unit : owner.getUnitsInEventOrder()) {
			if (unit.zone == GameState.Zone.LEFT || unit.zone == GameState.Zone.RIGHT) {
				if (!unit.getBool("aggressive")) {
					unit.attrs.addChanger("aggressive", this, false, true);
				}
			}
		}
	}
	
	@Override
	public void endTurnAction() {
		super.endTurnAction();
		for (UnitCard unit : owner.gameState.otherPlayer.getUnitsInEventOrder()) {
			if (unit.zone == GameState.Zone.LEFT || unit.zone == GameState.Zone.RIGHT) {
				if (!unit.getBool("aggressive")) {
					unit.attrs.addChanger("aggressive", this, true, false);
				}
			}
		}
	}

}

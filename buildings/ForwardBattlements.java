package buildings;

import gameServer.BuildingCard;
import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.Player;
import gameServer.UnitCard;

public class ForwardBattlements extends BuildingCard {

	public ForwardBattlements(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Forward Battlements";
		description = "Deployment: All units in play with Defensive lose Defensive";
		attrs.set("cost", 100);
	}

	@Override
	public void isDeployed() {
		for ( CardInfo card : GameState.state.allCards.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.getBool("defensive")) {
					unit.attrs.addChanger("defensive", this, false, false, 0);
				}
			}
		}
	}

}

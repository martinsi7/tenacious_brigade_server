package buildings;

import gameServer.BuildingCard;
import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.GameState.Zone;
import gameServer.Player;
import gameServer.UnitCard;

public class CaptainBobsDistressBeacon extends BuildingCard {

	public CaptainBobsDistressBeacon(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Captain X's Distress Beacon";
		description = "Deployment: Move all units to the center Lane. " +
				"Units with Mobility cannot move again this turn";
		attrs.set("cost", 100);
	}

	@Override
	public void isDeployed() {
		for ( CardInfo card : GameState.state.allCards.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.zone != Zone.CENTER) {
					zone = Zone.CENTER;
				}
				unit.hasMoved = false;
				unit.dirty = true;
			}
		}
	}

}

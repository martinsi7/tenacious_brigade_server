package buildings;

import gameServer.BuildingCard;
import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.Player;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class PartyCentral extends BuildingCard {

	public PartyCentral(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Party Central";
		description = "Deployment: All Fighters have their attack reduced to 0";
		attrs.set("cost", 100);
	}

	@Override
	public void isDeployed() {
		for ( CardInfo card : GameState.state.allCards.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.unitType == UnitType.FIGHTER) {
					int attack = unit.attrs.getOrig("attack");
					unit.attrs.addChanger("attack", this, false, false, -1 * attack);
				}
			}
		}
	}

}

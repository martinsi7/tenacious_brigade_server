package buildings;

import gameServer.BuildingCard;
import gameServer.Player;
import gameServer.UnitCard;

public class BobsPavillion extends BuildingCard {

	public BobsPavillion(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Grandpa No Arm's Quarters";
		description = "Deployment: All of your allied units gain +0/+1";
		attrs.set("cost", 50);
	}

	@Override
	public void isDeployed() {
		for ( UnitCard unit : owner.getUnitsInEventOrder()) {
			unit.attrs.addChanger("health", this, false, false, 1);
		}
	}

}

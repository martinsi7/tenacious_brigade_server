package buildings;

import gameServer.BuildingCard;
import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class BionicsFactory extends BuildingCard {

	public BionicsFactory(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Bionics Factory";
		description = "When built, all units in play with power " +
				"5 or greater gain Mechanized";
		attrs.set("cost", 100);
		
	}

	@Override
	public void isDeployed() {
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.attrs.get("attack") > 5 && !unit.getBool("mechanized")) {
					unit.attrs.addChanger("mechanized", this, false, false);
				}
			}
		}
	}

}

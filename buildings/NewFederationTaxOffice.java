package buildings;

import gameServer.BuildingCard;
import gameServer.Player;

public class NewFederationTaxOffice extends BuildingCard {
	
	private boolean otherPlayerChanged = false;

	public NewFederationTaxOffice(Player player) {
		super(player);
	}

	@Override
	public void isDeployed() {
		owner.maxCreditsPerTurn = 10;
	}

	@Override
	public void setup() {
		name = "New Federation Tax Office";
		description = "players earn a maximum of $50 per turn";
		attrs.set("cost", 100);
	}
	
	@Override
	public void endTurnAction() {
		super.endTurnAction();
		if (otherPlayerChanged) {
			return;
		}
		if (owner.gameState.otherPlayer.deck.building.isDeployed) {
			owner.gameState.otherPlayer.maxCreditsPerTurn = 10;
			otherPlayerChanged = true;
		}
	}

}

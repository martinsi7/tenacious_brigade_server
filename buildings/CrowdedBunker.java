package buildings;

import gameServer.BuildingCard;
import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.Player;
import gameServer.UnitCard;

public class CrowdedBunker extends BuildingCard {

	public CrowdedBunker(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Crowded Bunker";
		description = "Deployment: All Fighters in play gain Tenacity and Berserker";
		attrs.set("cost", 100);
	}

	@Override
	public void isDeployed() {
		for ( CardInfo card : GameState.state.allCards.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (!unit.getBool("tenacity")) {
					unit.attrs.addChanger("tenacity", this, false, false);
				}
				if (!unit.getBool("berserker")) {
					unit.attrs.addChanger("berserker", this, false, false);
				}
			}
		}
	}

}

package buildings;

import gameServer.BuildingCard;
import gameServer.Player;

public class BobsQuarters extends BuildingCard {

	public BobsQuarters(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Grandpa No Arm's Quarters";
		description = "Deployment: Gain 5 life";
		attrs.set("cost", 50);
	}

	@Override
	public void isDeployed() {
		owner.health += 5;
		owner.dirty = true;
	}

}

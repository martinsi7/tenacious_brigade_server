package buildings;

import gameServer.BuildingCard;
import gameServer.Player;
import gameServer.UnitCard;

public class BobsTent extends BuildingCard {

	public BobsTent(Player player) {
		super(player);
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void setup() {
		name = "Grandpa No Arm's Tent";
		description = "Deployment: Your highest health unit gains +3/+3";
		attrs.set("cost", 50);
	}

	@Override
	public void isDeployed() {
		UnitCard target = owner.getMostUnit("health", true);
		if (target != null) {
			target.attrs.addChanger("attack", this, false, false, 3);
			target.attrs.addChanger("health", this, false, false, 3);
		}
	}

}

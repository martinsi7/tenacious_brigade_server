package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class AllOwnedFighterCooldown extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.owner != owner.owner) {
			return false;
		}
		if (!newUnit.isCooldown) {
			return false;
		}
		if (newUnit.unitType != UnitType.FIGHTER) {
			return false;
		}
		return true;
	}

}

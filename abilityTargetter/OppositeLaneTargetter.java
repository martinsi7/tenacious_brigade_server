package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

public class OppositeLaneTargetter extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (owner instanceof UnitCard) {
			UnitCard unit = (UnitCard) owner;
			if (unit.zone == newUnit.zone &&
				unit.owner != newUnit.owner) {
				return true;
			}
		}
		return false;
	}

}

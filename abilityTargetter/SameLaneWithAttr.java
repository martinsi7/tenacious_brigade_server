package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

public class SameLaneWithAttr extends AbilityTargetter {
	
	private String attr;

	public SameLaneWithAttr(String _attr) {
		attr = _attr;
	}

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.zone == owner.zone && newUnit.owner == owner.owner
				&& newUnit.getBool(attr)) {
			return true;
		} else {
			return false;
		}
	}

}

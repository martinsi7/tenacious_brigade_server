package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class SameLaneFighter extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.zone == owner.zone &&
			newUnit.unitType == UnitType.FIGHTER) {
			return true;
		}
		return false;
	}

}

package abilityTargetter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import gameServer.CardInfo;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class TwoHighestAttackFighters extends AbilityTargetter {
public CardInfo.Faction faction;
	
	public TwoHighestAttackFighters(CardInfo.Faction _faction) {
		faction = _faction;
	}

	@Override
	public ArrayList<UnitCard> getTargets(CardInfo owner) {
		ArrayList<UnitCard> candidates = new ArrayList<UnitCard>();
		ArrayList<UnitCard> targets = new ArrayList<UnitCard>();
		for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (shouldAdd(unit, owner)) {
					candidates.add(unit);
				}
			}
		}
		Collections.sort(candidates, new Comparator<UnitCard>(){
			public int compare(UnitCard o1, UnitCard o2){
				return o2.getAttr("attack") - o1.getAttr("attack");
			}
		});
		if (candidates.size() > 0) {
			targets.add(candidates.get(0));
		} 
		if (candidates.size() > 1) {
			targets.add(candidates.get(1));
		}
		return targets;
	}

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.owner != owner.owner &&
			newUnit.unitType == UnitType.FIGHTER) {
			return true;
		}
		return false;
	}

}

package abilityTargetter;

import java.util.ArrayList;

import gameServer.CardInfo;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class HighestAttackFighter extends AbilityTargetter {
	
	@Override
	public ArrayList<UnitCard> getTargets(CardInfo owner) {
		int maxAttack = -1;
		UnitCard finalTarget = null;
		ArrayList<UnitCard> targets = new ArrayList<UnitCard>();
		for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (shouldAdd(unit, owner) && unit.attrs.get("attack") > maxAttack) {
					finalTarget = unit;
					maxAttack = unit.getAttr("attack");
				}
			}
		}
		if (finalTarget != null) {
			targets.add(finalTarget);
		}
		return targets;
	}

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.owner == owner.owner &&
			newUnit.unitType == UnitType.FIGHTER) {
			return true;
		}
		return false;
	}

}

package abilityTargetter;

import java.util.ArrayList;

import gameServer.CardInfo;
import gameServer.UnitCard;

public class SameLaneHighestHealthFighter extends AbilityTargetter {
	
	@Override
	public ArrayList<UnitCard> getTargets(CardInfo owner) {
		int maxHealth = 0;
		UnitCard candidate = null;
		ArrayList<UnitCard> targets = new ArrayList<UnitCard>();
		for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (shouldAdd(unit, owner)) {
					if (unit.getAttr("health") > maxHealth) {
						maxHealth = unit.getAttr("health");
						candidate = unit;
					}
				}
			}
		}
		if (candidate != null) {
			targets.add(candidate);
		}
		return targets;
	}

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.zone == owner.zone && newUnit.owner == owner.owner
				&& newUnit.unitType == UnitCard.UnitType.FIGHTER) {
			return true;
		} else {
			return false;
		}
	}

}

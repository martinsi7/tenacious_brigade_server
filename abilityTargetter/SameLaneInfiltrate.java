package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

public class SameLaneInfiltrate extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.zone == owner.zone &&
				newUnit.owner == owner.owner &&
				newUnit.getBool("infiltrate")) {
			return true;
		} else {
			return false;
		}
	}

}

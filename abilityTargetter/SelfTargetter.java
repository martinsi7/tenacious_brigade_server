package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

public class SelfTargetter extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.gameID == owner.gameID) {
			return true;
		} else {
			return false;
		}
	}

}

package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

import java.util.ArrayList;

public abstract class AbilityTargetter {
	public ArrayList<UnitCard> getTargets(CardInfo owner) {
		ArrayList<UnitCard> targets = new ArrayList<UnitCard>();
		for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (shouldAdd(unit, owner)) {
					targets.add(unit);
				}
			}
		}
		return targets;
	}
	
	protected abstract boolean shouldAdd(UnitCard newUnit, CardInfo owner);
}

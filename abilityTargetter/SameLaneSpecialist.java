package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;
import gameServer.UnitCard.UnitType;

public class SameLaneSpecialist extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (newUnit.zone == owner.zone &&
				newUnit.unitType == UnitType.SPECIALIST) {
				return true;
			}
			return false;
	}

}

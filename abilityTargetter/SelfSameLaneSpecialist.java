package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

import java.util.ArrayList;

public class SelfSameLaneSpecialist extends AbilityTargetter {

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		return false;
	}
	
	@Override
	public ArrayList<UnitCard> getTargets(CardInfo owner) {
		ArrayList<UnitCard> targets = new ArrayList<UnitCard>();
		for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
			if (card.zone == owner.zone &&
					card.gameID != owner.gameID &&
					card.attrs.getBool("mobility")) {
				targets.add((UnitCard) owner);
			}
		}
		return targets;
	}

}

package abilityTargetter;

import gameServer.CardInfo;
import gameServer.UnitCard;

public class AllOwnedTargetter extends AbilityTargetter {
	public CardInfo.Faction faction;
	
	private String attrFilter;
	private UnitCard.UnitType unitType = UnitCard.UnitType.NONE;
	
	public AllOwnedTargetter(CardInfo.Faction _faction) {
		faction = _faction;
		attrFilter = "";
	}
	
	public AllOwnedTargetter(CardInfo.Faction _faction, String attr) {
		faction = _faction;
		attrFilter = attr;
	}
	
	public AllOwnedTargetter(CardInfo.Faction _faction, String attr, UnitCard.UnitType _unitType) {
		faction = _faction;
		attrFilter = attr;
		unitType = _unitType;
	}

	@Override
	protected boolean shouldAdd(UnitCard newUnit, CardInfo owner) {
		if (faction != CardInfo.Faction.NEUTRAL &&
				faction != CardInfo.Faction.NONE &&
				newUnit.faction != faction) {
			return false;
		}
		if (attrFilter != "" && !newUnit.getBool(attrFilter)) {
			return false;
		}
		if (unitType != UnitCard.UnitType.NONE && unitType != newUnit.unitType) {
			return false;
		}
		if (newUnit.owner == owner.owner) {
			return true;
		} else {
			return false;
		}
	}

}

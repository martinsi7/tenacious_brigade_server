package anarchistYouthCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetChangeAbilities;

public class SteroidDealer extends UnitCard {

	public SteroidDealer(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Steroid Dealer";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		TargetChangeAbilities ability = new TargetChangeAbilities(0, this, 1, targetter, "attack", 2, true, true, false); 
		ability.description = "cooldown: target unit in this lane gets +2/+0 until the beginning of your next turn.";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

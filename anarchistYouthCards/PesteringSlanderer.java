package anarchistYouthCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class PesteringSlanderer extends UnitCard {

	public PesteringSlanderer(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Pestering Slanderer";
		description = "Deployment: all fighters in opposing " +
				"lane gain -2/-0 and beserker permanently";
		attrs.set("attack", 2);
		attrs.set("health", 2);
		attrs.set("cost", 40);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner != owner && unit.zone == zone
						&& unit.unitType == UnitType.SPECIALIST) {
					unit.attrs.change("attack", -2);
					if (unit.attrs.get("attack") < 0) {
						unit.attrs.set("attack", 0);
					}
					attrs.setTrue("berserker");
					unit.dirty = true;
				}
			}
		}
	}

}

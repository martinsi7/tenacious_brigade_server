package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class EnragedRadical extends UnitCard {

	public EnragedRadical(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Insightful Radical";
		description = "Berserker, Aggressive, Mechanized.";
		attrs.set("attack", 3);
		attrs.set("health", 7);
		attrs.set("cost", 70);
		attrs.set("berserker", true);
		attrs.set("aggressive", true);
		attrs.set("mechanized", true);
		unitType = UnitType.FIGHTER;
	}

}

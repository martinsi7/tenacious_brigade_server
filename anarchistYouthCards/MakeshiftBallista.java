package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class MakeshiftBallista extends UnitCard {

	public MakeshiftBallista(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Makeshift Ballista";
		description = "mobility, shambling";
		attrs.set("attack", 8);
		attrs.set("health", 2);
		attrs.set("cost", 60);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("mobility");
		attrs.setTrue("shambling");
	}

}

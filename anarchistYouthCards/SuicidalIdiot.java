package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class SuicidalIdiot extends UnitCard {

	public SuicidalIdiot(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Suicidal Idiot";
		description = "Berserker";
		attrs.set("attack", 6);
		attrs.set("health", 1);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("berserker");
	}

}

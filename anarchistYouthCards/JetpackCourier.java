package anarchistYouthCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetMultipleAbilities;

public class JetpackCourier extends UnitCard {

	public JetpackCourier(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Jetpack Courier";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		String[] attrs = {"attack", "mobility"};
		int[] amounts = {1};
		TargetMultipleAbilities ability = new  TargetMultipleAbilities(20, this, 1, targetter, false, attrs, amounts, false, true);
		ability.description = "$20: target unit in this lane gets +1/+0 and mobility until end of turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

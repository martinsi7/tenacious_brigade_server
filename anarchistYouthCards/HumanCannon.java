package anarchistYouthCards;

import gameServer.ActiveAbility;
import gameServer.MechCardInfo;
import gameServer.Player;

import java.util.ArrayList;

import abilityTargetter.AllOwnedTargetter;
import activeAbilities.TargetChangeAbilities;
import cardStatus.StatusChanger;

public class HumanCannon extends MechCardInfo {

	public HumanCannon(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		name = "Human Cannon";
		description = "";
		attrs.set("cost", 40);
		attrs.set("attack", 1);
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AllOwnedTargetter targetter = new AllOwnedTargetter(Faction.ANARCHIST_YOUTH);
		StatusChanger changer = new StatusChanger(this, false, true);
		TargetChangeAbilities ability = new TargetChangeAbilities(20, this, 2, targetter, "mobility", changer, false);
		ability.description = "$20: give 2 target Anarchist Youth units " +
				"mobility until end of turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

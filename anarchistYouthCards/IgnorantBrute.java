package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class IgnorantBrute extends UnitCard {

	public IgnorantBrute(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Ignorant Brute";
		description = "aggresive";
		attrs.set("attack", 4);
		attrs.set("health", 2);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("aggressive");
	}

}

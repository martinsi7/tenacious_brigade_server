package anarchistYouthCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class LazyLooter extends UnitCard {

	public LazyLooter(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Lazy Looter";
		description = "Deployment: this unit gets +2/+1 if it's the only friendly" +
				" unit in this lane";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 20);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		boolean sharedLane = false;
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card != this && card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner == owner && unit.zone == zone) {
					sharedLane = true;
					break;
				}
			}
		}
		if (!sharedLane) {
			attrs.change("attack", 2);
			attrs.change("health", 1);
			dirty = true;
		}
	}

}

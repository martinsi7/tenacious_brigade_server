package anarchistYouthCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class AggravatingSlanderer extends UnitCard {

	public AggravatingSlanderer(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Aggravating Slanderer";
		description = "Deployment: all specialists in opposing " +
				"lane gain +1/-0 and beserker permanently";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner != owner && unit.zone == zone
						&& unit.unitType == UnitType.SPECIALIST) {
					attrs.change("attack", 1);
					attrs.setTrue("berserker");
					unit.dirty = true;
				}
			}
		}
	}
	
}

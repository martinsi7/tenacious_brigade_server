package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class BelligerentRadical extends UnitCard {

	public BelligerentRadical(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Belligerent Radical";
		description = "Berserker";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 10);
		attrs.set("berserker", true);
		unitType = UnitType.FIGHTER;
	}

}

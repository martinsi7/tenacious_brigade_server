package anarchistYouthCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SelfTargetter;
import activeAbilities.TargetMultipleAbilities;

public class SteadfastRadical extends UnitCard {

	public SteadfastRadical(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Steadfast Radical";
		description = "Berserker, Regeneration.";
		attrs.set("attack", 3);
		attrs.set("health", 5);
		attrs.set("cost", 50);
		attrs.set("berserker", true);
		attrs.set("regerneration", true);
		unitType = UnitType.FIGHTER;
		setupActiveAbilities();
	}
	
	
	private void setupActiveAbilities(){
		SelfTargetter targetter = new SelfTargetter();
		String[] attrs = {"attack", "health"};
		int[] amounts = {-2, 1};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(0, this, 1, targetter, false, attrs, amounts, false, false);
		ability.description = "$0: gain -2/+1.";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class InsightfulRadical extends UnitCard {

	public InsightfulRadical(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Insightful Radical";
		description = "Berserker, Reach.";
		attrs.set("attack", 3);
		attrs.set("health", 3);
		attrs.set("cost", 30);
		attrs.set("berserker", true);
		attrs.set("reach", true);
		unitType = UnitType.FIGHTER;
	}

}

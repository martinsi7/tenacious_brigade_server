package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class AngryJunkie extends UnitCard {

	public AngryJunkie(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Angry Junkie";
		description = "mobility, shambling";
		attrs.set("attack", 3);
		attrs.set("health", 1);
		attrs.set("cost", 20);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("mobility");
		attrs.setTrue("shambling");
	}

}

package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class SparkOfRiot extends UnitCard {

	public SparkOfRiot(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Spark of Riot";
		description = "contract 10. Shambling. " +
				"When this unit takes combat damage when " +
				"it has shambling, it loses shambling and " +
				"gains +1/+2 and beserker";
		attrs.set("attack", 2);
		attrs.set("health", 4);
		attrs.set("cost", 20);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("shambling");
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 10;
		owner.dirty = true;
	}
	
	@Override
	public void takeDamage(int amount) {
		if (amount > 0 && getBool("shambling")) {
			attrs.set("berserker", true);
			attrs.change("attack", 1);
			attrs.change("health", 2);
			dirty = true;
		}
		super.takeDamage(amount);
		
	}

}

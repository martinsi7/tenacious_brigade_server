package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class EagerRebel extends UnitCard {

	public EagerRebel(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Eager Rebel";
		description = "mobility";
		attrs.set("attack", 2);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("mobility");
	}

}

package anarchistYouthCards;

import gameServer.CardInfo;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class VolatileFlamejets extends MechCardInfo {

	public VolatileFlamejets(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		name = "Volatile Flamejets";
		description = "At the beginning of your turn, if you do not have an " +
				"Anarchist Youth unit in play, gain debt 10";
		attrs.set("cost", 10);
		attrs.set("attack", 1);
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card != this && card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner == owner && unit.faction == UnitCard.Faction.ANARCHIST_YOUTH) {
					return;
				}
			}
		}
		owner.debt += 10;
		owner.dirty = true;
	}

}

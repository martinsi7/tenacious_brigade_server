package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class TemperamentalDrone extends UnitCard {

	public TemperamentalDrone(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Temperamental Drone";
		description = "Shambling, Mobility<br/>If this unit takes " +
				"combat damage, it gains +3/+0 and loses mobility";
		attrs.set("attack", 0);
		attrs.set("health", 5);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("shambling");
		attrs.setTrue("mobility");
	}
	
	@Override
	public void takeDamage(int amount) {
		if (amount > 0) {
			attrs.change("attack", 3);
			attrs.setFalse("mobility");
		}
		super.takeDamage(amount);
	}

}

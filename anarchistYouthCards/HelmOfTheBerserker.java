package anarchistYouthCards;

import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.AllOwnedTargetter;
import activeAbilities.TargetChangeAbilities;

public class HelmOfTheBerserker extends MechCardInfo {

	public HelmOfTheBerserker(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		name = "Helm of the Berserker";
		description = "At the beginning of your turn, all units you " +
				"control with berserker gain +1/+0 until the beginning " +
				"of your next turn.";
		attrs.set("cost", 50);
		setActiveAbilities();
	}

	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.isCooldown) {
					continue;
				}
				if (unit.owner != owner && unit.getBool("berserker")) {
					unit.attrs.addChanger("attack", this, true, false, 1);
				}
			}
		}
	}
	
	private void setActiveAbilities(){
		AllOwnedTargetter targetter1 = new AllOwnedTargetter(CardInfo.Faction.ANARCHIST_YOUTH);
		TargetChangeAbilities ability1 = new TargetChangeAbilities(10, this, 1, targetter1, "berserker", false, false, false);
		ability1.description = "$10: give target Anarchist Youth unit berserker.";
		AllOwnedTargetter targetter2 = new AllOwnedTargetter(CardInfo.Faction.NONE, "berserker");
		TargetChangeAbilities ability2 = new TargetChangeAbilities(30, this, 1, targetter2, "health", 1, false, false, false);
		ability2.description = "$30: give target unit with berserker +0/+1.";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability1);
		activeAbilities.add(ability2);
	}
}

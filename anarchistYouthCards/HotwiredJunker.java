package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class HotwiredJunker extends UnitCard {

	public HotwiredJunker(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Hotwired Junker";
		description = "Aggressive, Shambling<br/>If this unit takes combat damage, it loses aggressive";
		attrs.set("attack", 4);
		attrs.set("health", 7);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
		attrs.setTrue("shambling");
		attrs.setTrue("aggressive");
	}
	
	@Override
	public void takeDamage(int amount) {
		if (amount > 0) {
			attrs.set("aggressive", false);
		}
		super.takeDamage(amount);
	}

}

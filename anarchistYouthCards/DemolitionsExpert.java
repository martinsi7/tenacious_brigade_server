package anarchistYouthCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class DemolitionsExpert extends UnitCard {

	public DemolitionsExpert(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Demolitions Expert";
		description = "At the beginning of your turn, if there is a specialist in the " +
				"opposing lane, this unit gains mobility until end of turn.";
		attrs.set("attack", 6);
		attrs.set("health", 1);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
	}
	
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner != owner && unit.unitType == UnitType.SPECIALIST) {
					attrs.addChanger("mobility", null, false, true);
					return;
				}
			}
		}
	}
	
}


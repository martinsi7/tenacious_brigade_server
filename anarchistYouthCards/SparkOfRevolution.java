package anarchistYouthCards;

import gameServer.Player;
import gameServer.UnitCard;

public class SparkOfRevolution extends UnitCard {

	public SparkOfRevolution(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Spark of Revolution";
		description = "contract 20. Shambling. " +
				"When this unit takes combat damage when " +
				"it has shambling, it loses shambling and " +
				"gains +2/+3 and beserker";
		attrs.set("attack", 3);
		attrs.set("health", 7);
		attrs.set("cost", 50);
		unitType = UnitType.FIGHTER;
		attrs.set("shambling", true);
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 20;
		owner.dirty = true;
	}
	
	@Override
	public void takeDamage(int amount) {
		if (amount > 0 && getBool("shambling")) {
			attrs.set("berserker", true);
			attrs.change("attack", 2);
			attrs.change("health", 3);
			dirty = true;
		}
		super.takeDamage(amount);
		
	}

}

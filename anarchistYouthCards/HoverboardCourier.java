package anarchistYouthCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class HoverboardCourier extends UnitCard {

	public HoverboardCourier(Player player) {
		super(player);
		faction = Faction.ANARCHIST_YOUTH;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Hoverboard Courier";
		description = "At the beginning of your turn, the unit with " +
				"the highest attack in this lane gains mobility until " +
				"the beginning of your next turn.";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		UnitCard target = null;
		int highestAttack = -1;
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.isCooldown) {
					continue;
				}
				if (unit.getAttack() > highestAttack) {
					highestAttack = unit.getAttack();
					target = unit;
				}
			}
		}
		if (target != null) {
			target.attrs.addChanger("mobility", this, false, true);
		}
	}

}

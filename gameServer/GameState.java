package gameServer;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GameState {
	public Player 						curPlayer;
	public Player 						otherPlayer;
	public int 							turn;
	public HashMap<String, CardInfo>	allCards;
	public HashMap<String, CardInfo>	cardsInPlay;
	public Phase						curPhase;
	public boolean 						needsTargets;
	public String 						targeterID;
	public ArrayList<String>			validTargetIDs;
	public CardAbility					waitingAction;
	public HashSet<String>				attackingCards;
	public boolean						hasChanges = false;
	public boolean 						showCombatChanges = false;
	public JSONArray					combatResults;
	public String 						requester = "";
	
	private Player 						playerOne;
	private Player 						playerTwo;
	
	public GameState(String deckOne, String deckTwo){
		state = this;
	}
	
	public static GameState state;
	public GameState(){
		allCards = new HashMap<String, CardInfo>();
		cardsInPlay = new HashMap<String, CardInfo>();
		attackingCards = new HashSet<String>();
		turn = 1;
		curPhase = Phase.FIRSTMAIN;
		waitingAction = null;
		state = this;
		combatResults = new JSONArray();
	}
	
	public void init(String deckOne, String deckTwo){
		int INITIAL_HAND_SIZE = 5;
		playerOne = new Player(deckOne, this);
		playerTwo = new Player(deckTwo, this);
		playerTwo.bank = 10;
		playerOne.bank = 10;
		playerOne.name = "one";
		playerTwo.name = "two";
		playerOne.dirty = true;
		playerTwo.dirty = true;
		curPlayer = playerOne;
		otherPlayer = playerTwo;
		fillAllCards(playerOne.deck);
		fillAllCards(playerTwo.deck);
		for (int i = 0; i < INITIAL_HAND_SIZE - 1; i++) {
			playerOne.drawCard();
			playerTwo.drawCard();
		}
		playerTwo.drawCard();
	}
	
	public void init(String[] deckOne, String[] deckTwo) {
		int INITIAL_HAND_SIZE = 5;
		playerOne = new Player(deckOne, this);
		playerTwo = new Player(deckTwo, this);
		playerTwo.bank = 10;
		playerOne.bank = 10;
		playerOne.name = "one";
		playerTwo.name = "two";
		playerOne.dirty = true;
		playerTwo.dirty = true;
		curPlayer = playerOne;
		otherPlayer = playerTwo;
		fillAllCards(playerOne.deck);
		fillAllCards(playerTwo.deck);
		for (int i = 0; i < INITIAL_HAND_SIZE - 1; i++) {
			playerOne.drawCard();
			playerTwo.drawCard();
		}
		playerTwo.drawCard();
	}
	
	public boolean isRequester(Player player) {
		if (requester.equals("one") && player == playerOne) {
			return true;
		}
		if (requester.equals("two") && player == playerTwo) {
			return true;
		}
		if (requester.equals("both")) {
			return true;
		}
		return false;
	}
	
	public void setAttackers(JSONArray cards){
		for(int i = 0; i < cards.length(); i++) {
			String cardID = cards.optString(i);
			attackingCards.add(cardID);
		}
		nextPhase();
	}
	
	public void resolveAllCombat(JSONArray combats){
		for (int i = 0; i < combats.length(); i++){
			combatResults = combats;
			JSONObject singleCombat = combats.optJSONObject(i);
			String attackerID = singleCombat.optString("attacker");
			if (attackerID == null){
				continue;
			}
			// TODO: check if attacker is in attackingCards HashSet
			Unit attacker = null;
			if (attackerID.contains("player-one")) {
				attacker = playerOne;
			} else if (attackerID.contains("player-two")) {
				attacker = playerTwo;
			} else {
				attacker = (UnitCard) allCards.get(attackerID);
			}
			ArrayList<Unit> defenders = new ArrayList<Unit>();
			// TODO: provide server side validation of attackers and defenders
			JSONArray defenderIDs = singleCombat.optJSONArray("defenders");
			if (defenderIDs != null){
				for (int j = 0; j < defenderIDs.length(); j++) {
					String defenderID = defenderIDs.optString(j);
					defenders.add((UnitCard) allCards.get(defenderID));
				}
			}
			resolveSingleCombat(attacker, defenders);
		}
		showCombatChanges = true;
		nextPhase();
	}
	
	private void resolveSingleCombat(Unit attacker, ArrayList<Unit> defenders) {
		if (defenders.size() == 0) {
			otherPlayer.takeDamage(attacker.getAttack());
			if (attacker.doesGiveDebt()) {
				otherPlayer.receiveDebt(attacker.getDebtAmount());
			}
		} else {
			int totalDamage = 0;
			int snipeAmount = 0;
			boolean defenderGivenDebt = false;
			for (int i = 0; i < defenders.size(); i++){
				Unit defender = defenders.get(i);
				if (attacker.doesGiveDebt() && !defenderGivenDebt) {
					defender.receiveDebt(attacker.getDebtAmount());
					defenderGivenDebt = true;
				}
				if (defender.doesGiveDebt()) {
					attacker.receiveDebt(defender.getDebtAmount());
				}
				if (defender.isMechanized()) {
					attacker.takeDamage(defender.getAttack());
				} else {
					totalDamage += defender.getAttack();
				}
				snipeAmount = Math.max(snipeAmount, defender.getSnipeAmount()); 
				if (attacker.isMechanized()) {
					defender.takeDamage(attacker.getAttack());
				} else {
					defender.getAttacked(attacker.getAttack(), attacker.getSnipeAmount());
				}
			}
			attacker.getAttacked(totalDamage, snipeAmount);
			
		}
	}
	
	public void nextPhase(){
		switch (curPhase) {
		case FIRSTMAIN:
			curPhase = Phase.ATTACK;
			break;
		case ATTACK:
			curPhase = Phase.DEFEND;
			break;
		case DEFEND:
			attackingCards.clear();
			curPhase = Phase.SECONDMAIN;
			break;
		case SECONDMAIN:
			changeTurn();
			break;
		default:
			break;
		}
	}
	
	/*private void doAllTempActions(ArrayList<TempAction> actions) {
		for (TempAction action : actions) {
			action.undoChange();
		}
		actions.clear();
	}*/
	
	public void changeTurn(){
		ArrayList<CardInfo> cards = curPlayer.getCardsInEventOrder();
		for (int i=0; i<cards.size(); i++) {
			CardInfo card = cards.get(i);
			if (cardsInPlay.containsKey(card.gameID)) {
				card.updateAttrs(true, false);
				card.doEndTurnAction();
				if (card.isDead()) {
					card.goToClinic();
				}
			}
		}
		
		curPhase = Phase.FIRSTMAIN;
		if (curPlayer.debt > 0) {
			curPlayer.takeDamage(curPlayer.debt / 10);
		}
		if (curPlayer == playerOne) {
			curPlayer = playerTwo;
			otherPlayer = playerOne;
		} else {
			turn++;
			curPlayer = playerOne;
			otherPlayer = playerTwo;
		}
		curPlayer.drawCard();
		curPlayer.earnCredits(turn);
		curPlayer.dirty = true;
		curPlayer.deck.incrementBuildingProgress();
		lowerCooldownAndPrep();
		
		cards = curPlayer.getCardsInEventOrder();
		for (int i=0; i<cards.size(); i++) {
			CardInfo card = cards.get(i);
			if (cardsInPlay.containsKey(card.gameID)) {
				card.updateAttrs(false, true);
				card.doStartTurnAction();
				if (card.isDead()) {
					card.goToClinic();
				}
			}
		}
	}
	
	private void lowerCooldownAndPrep(){
		for (CardInfo card : cardsInPlay.values()){
			UnitCard unitCard = UnitCard.asUnit(card);
			if (unitCard != null && unitCard.owner == curPlayer){
				unitCard.recoverCooldown();
			}
		}
	}
	
	private void fillAllCards(Deck deck){
		for (int i = 0; i < deck.cards.size(); i++){
			CardInfo card = deck.cards.get(i);
			allCards.put(card.gameID, card);
		}
	}
	
	public JSONObject serializeDirtyAndClean() {
		return serializeDirtyAndClean(false);
	}
	
	public JSONObject serializeDirtyAndClean(boolean isTurnChange) {
		JSONObject retObj = new JSONObject();
		try {
			retObj.putOpt("players", serializeDirtyAndCleanPlayers());
			retObj.putOpt("cards", serializeDirtyAndCleanCards(isTurnChange));
			hasChanges = false;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return retObj;
	}
	
	private JSONArray serializeDirtyAndCleanCards(boolean isTurnChange) {
		JSONArray retArr = new JSONArray();
		for (CardInfo card : curPlayer.getCardsInEventOrder()) {
			if (card.dirty || (isTurnChange && card.endDirty)) {
				retArr.put(card.serialize(isTurnChange, false, false));
				card.dirty = false;
			}
		}
		for (CardInfo card : otherPlayer.getCardsInEventOrder()) {
			if (card.dirty || (isTurnChange && card.beginningDirty)) {
				retArr.put(card.serialize(false, isTurnChange, false));
				card.dirty = false;
			}
		}
		return retArr;
	}
	
	private JSONArray serializeDirtyAndCleanPlayers() {
		JSONArray retArr = new JSONArray();
		if (curPlayer.dirty) {
			retArr.put(curPlayer.serialize());
			curPlayer.dirty = false;
		}
		if (otherPlayer.dirty) {
			retArr.put(otherPlayer.serialize());
			otherPlayer.dirty = false;
		}
		return retArr;
	}
	
	public JSONObject serializeDirty() {
		return serializeDirty(false);
	}
	
	public JSONObject serializeDirty(boolean isTurnChange) {
		JSONObject retObj = new JSONObject();
		try {
			retObj.putOpt("players", serializeDirtyPlayers());
			retObj.putOpt("cards", serializeDirtyCards(isTurnChange));
			hasChanges = true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return retObj;
	}
	
	private JSONArray serializeDirtyCards(boolean isTurnChange) {
		JSONArray retArr = new JSONArray();
		for (CardInfo card : otherPlayer.getCardsInEventOrder()) {
			if (card.dirty || (isTurnChange && card.endDirty)) {
				retArr.put(card.serialize(isTurnChange, false, false));
			}
		}
		for (CardInfo card : curPlayer.getCardsInEventOrder()) {
			if (card.dirty || (isTurnChange && card.beginningDirty)) {
				retArr.put(card.serialize(false, isTurnChange, false));
			}
		}
		return retArr;
	}
	
	private JSONArray serializeDirtyPlayers() {
		JSONArray retArr = new JSONArray();
		if (curPlayer.dirty) {
			retArr.put(curPlayer.serialize());
		}
		if (otherPlayer.dirty) {
			retArr.put(otherPlayer.serialize());
		}
		return retArr;
	}
	
	public enum Phase {
		FIRSTMAIN, ATTACK, DEFEND, SECONDMAIN, NONE
	}
	
	public static String prettifyPhaseName(Phase phase) {
		switch (phase) {
		case ATTACK:
			return "Attack Phase";
		case DEFEND:
			return "Defense Phase";
		case FIRSTMAIN:
			return "Main Phase One";
		case NONE:
			return "";
		case SECONDMAIN:
			return "Main Phase Two";
		default:
			return "";
		
		}
	}
	
	
	public enum Zone {
		DECK, HAND, MECH, LEFT, CENTER, RIGHT, CLINIC, BUILDING, NONE
	}
	
	public String zoneToString(Zone zone, Player player){
		String str = "";
		if (player == playerOne){
			str += "player-one-";
		} else if (player == playerTwo){
			str += "player-two-";
		}
		str += GameState.zoneToString(zone);
		return str;
	}
	
	public static Zone stringToZone(String zone) {
		String tempZone = new String(zone);
		if (tempZone.split("-").length == 3) {
			tempZone = tempZone.split("-")[2];
		}
		switch (tempZone) {
		case "deck":
			return Zone.DECK;
		case "hand":
			return Zone.HAND;
		case "mech":
			return Zone.MECH;
		case "left":
			return Zone.LEFT;
		case "center":
			return Zone.CENTER;
		case "right":
			return Zone.RIGHT;
		case "building":
			return Zone.BUILDING;
		default:
			System.err.println("unrecognized zone: " + zone);
			return Zone.NONE;
		}
	}
	
	public static String zoneToString(Zone zone){
		switch (zone) {
		case DECK:
			return "deck";
		case HAND:
			return "hand";
		case MECH:
			return "mech";
		case LEFT: 
			return "left";
		case CENTER:
			return "center";
		case RIGHT:
			return "right";
		case CLINIC:
			return "clinic";
		case BUILDING:
			return "building";
		default:
			return "";
		}
	}
	
	// currently this returns true if same zone && center, left, or right
	// it could be adapted to return false if same zone without breaking dependencies 
	public static boolean isAdjacentZone(Zone zone, Zone other) {
		switch (zone) {
		case LEFT:
			if (other == Zone.CENTER || other == Zone.LEFT) {
				return true;
			}
			break;
		case CENTER:
			if (other == Zone.CENTER || other == Zone.LEFT
				|| other == Zone.RIGHT) {
				return true;
			}
			break;
		case RIGHT:
			if (other == Zone.CENTER || other == Zone.RIGHT) {
				return true;
			}
		default:
			return false;
		}
		return false;
	}
	
	public JSONObject activateAbility(String cardID, int index) {
		CardInfo card = cardsInPlay.get(cardID);
		if (card == null) {
			System.err.println("card not found with card id " + cardID);
			return null;
		}
		if (card instanceof UnitCard) {
			UnitCard unit = (UnitCard) card;
			if (unit.isCooldown) {
				return null;
			}
		}
		CardAbility ability = card.activeAbilities.get(index);
		if (ability == null) {
			System.err.println("ability not found for card id " + cardID + " at index " + index);
			return null;
		}
		TargetInfo targetInfo = ability.doEventAction();
		if (targetInfo == null) {
			return null;
		} else {
			waitingAction = ability;
			return targetInfo.toJson();
		}
	}
	
	public void doResolvedAction(TargetInfo targets) {
		if (waitingAction == null) {
			System.err.println("no waiting action found");
			return;
		}
		if (targets != null) {
			waitingAction.doResolvedAction(targets);
		}
		waitingAction = null;
	}
	
	/*public void doEventActions(ActionList actions) {
		curEvents = actions;
		while (actions.hasNextAction()) {
			TargetInfo targetInfo = actions.doNextAction();
			if (targetInfo != null) {
				// TODO: return with target info added to JSON somehow
			}
		}
		curEvents.reset();
		curEvents = null;
	}*/
	
	/*public void removeEventAbilities(CardInfo card) {
		playerOne.removeEventAbilites(card);
		playerTwo.removeEventAbilites(card);
	}*/
	
	public static String getIDString(){
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}
	
	public static ArrayList<String> JSONArrayToArrayList(JSONArray arr) {
		ArrayList<String> ret = new ArrayList<String>();
		try {
			if (arr != null) {
				for (int i=0; i<arr.length(); i++) {
					ret.add(arr.getString(i));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return ret;
	}
}

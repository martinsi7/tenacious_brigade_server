package gameServer;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

public class TargetInfo {
	public boolean 				hasTarget;
	public ArrayList<CardInfo> 	validTargets;
	public boolean 				canTargetOtherPlayer;
	public boolean 				canTargetSelfPlayer;
	public String 				ownerCardID;
	// if targets not chosen this is number of allowed targets
	public int 					numTargets;
	
	public TargetInfo() {
		makeDefault();
	}
	
	public TargetInfo(ArrayList<CardInfo> targets, boolean canTargetOther, boolean canTargetSelf, String ownerID) {
		validTargets = targets;
		canTargetOtherPlayer = canTargetOther;
		canTargetSelfPlayer = canTargetSelf;
		hasTarget = true;
		ownerCardID = ownerID;
	}
	
	public JSONObject toJson() {
		JSONObject obj = new JSONObject();
		try {
			obj.put("hasTarget", hasTarget);
			obj.put("canTargetOtherPlayer", canTargetOtherPlayer);
			obj.put("canTargetSelfPlayer", canTargetSelfPlayer);
			obj.put("validTargets", getTargetIDs().toArray());
			obj.put("ownerCardID", ownerCardID);
			obj.put("numTargets", numTargets);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}
	
	private void makeDefault() {
		hasTarget = false;
		validTargets = new ArrayList<CardInfo>();
		canTargetOtherPlayer = false;
		canTargetSelfPlayer = false;
		numTargets = 0;
	}
	
	private ArrayList<String> getTargetIDs() {
		ArrayList<String> arr = new ArrayList<String>();
		for (CardInfo card : validTargets) {
			arr.add(card.gameID);
		}
		return arr;
	}
	

}

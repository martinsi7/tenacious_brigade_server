package gameServer;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cardStatus.AttributeHash;

public abstract class CardInfo  {
	public boolean 						dirty;
	public boolean						beginningDirty;
	public boolean						endDirty;
	public String 						gameID;
	public String						name;
	public String						description;
	//public int 							cost;
	public GameState.Zone 				zone;
	//public int 							attack;
	public Player						owner;
	public ArrayList<ActiveAbility>		activeAbilities;
	//public ArrayList<PassiveAbility>	passiveAbilities;
	public boolean						isInPlay;
	public CardType						cardType;
	public Faction 						faction;
	
	public int 							zoneIndex;
	
	public AttributeHash				attrs;
	
	public CardInfo(Player player) {
		dirty = false;
		gameID = GameState.getIDString();
		zone = GameState.Zone.DECK;
		this.owner = player;
		isInPlay = false;
		faction = Faction.NONE;
		attrs = new AttributeHash(this);
		attrs.set("cost", 0);
		zoneIndex = -1;
		setup();
		//attrs.put("attack", new CardStatus(int.class, 0));
	}
	
	public int getAttr(String key) {
		return attrs.get(key);
	}
	
	public boolean getBool(String key) {
		return attrs.getBool(key);
	}
	
	public void updateAttrs(boolean isEndTurn, boolean isBeginningTurn) {
		int turn = -1;
		if (isEndTurn || isBeginningTurn) {
			turn = owner.gameState.turn;
		}
		for (String attrName : attrs.getKeys()) {
			attrs.updateStatus(attrName, turn, isEndTurn, isBeginningTurn);
		}
	}
	
	public boolean hasAttr(String attr) {
		return attrs.hasAttr(attr);
	}
	
	public boolean isDead(){
		return (hasAttr("health") && getAttr("health") <= 0);
	}
	
	public abstract void setup();
	
	public abstract void doStartTurnAction();
	
	public abstract void doEndTurnAction();
	
	public void customAbility(TargetInfo targetInfo) {}
	
	public String debug(){
		String str = "Card: " + gameID;
		str += "\n\t dirty: " + dirty;
		return str;
	}
	
	public void goToClinic(){
		setup();
		dirty = true;
		attrs.change("cost", 20);
		owner.clinic.add(this);
		owner.getZoneCards(zone).remove(this);
		owner.changeLane(this.gameID, "clinic");
		//int cost = attrs.get("cost");
		//if (cost > 100) {
		//	zone = GameState.Zone.NONE;
		//} else {
			zone = GameState.Zone.CLINIC;
		//}
		isDestroyed();
		owner.gameState.cardsInPlay.remove(gameID);
		
	}
	
	public JSONArray getAbilityJSON() {
		JSONArray abilityArr = new JSONArray();
		if (activeAbilities != null) {
			for (ActiveAbility ability : activeAbilities) {
				abilityArr.put(ability.description);
			}
		}
		
		return abilityArr;
	}
	
	//public JSONObject serialize(boolean isEndTurn, boolean isBeginningTurn) {
	//	return serialize(isEndTurn, isBeginningTurn, false);
	//}
	
	protected boolean isHidden(){
		//if (!owner.gameState.isRequester(owner) &&
		//		zone==Zone.HAND) {
		//	return true;
		//}
		return false;
	}
	
	public JSONObject serialize(boolean isEndTurn, boolean isBeginningTurn, boolean hide){
		updateAttrs(isEndTurn, isBeginningTurn);
		JSONObject retObj = new JSONObject();
		hide = isHidden();
		try {
			if (hide) {
				retObj.putOpt("gameID", gameID);
			} else {
			
				retObj.putOpt("gameID", gameID)
					  .putOpt("name", name)
					  .putOpt("description", description)
					  .putOpt("owner", owner.name)
					  .putOpt("zone", owner.gameState.zoneToString(zone,  owner))
					  .putOpt("description", description)
					  .put("isInPlay", isInPlay)
					  .put("abilities", getAbilityJSON())
					  .put("cardType", cardTypeToString(this.cardType))
					  .put("faction", FactionToString(faction));
				JSONArray attrArr = new JSONArray();
				for (String attrName : attrs.getKeys()) {
					//CardStatus attr = attrs.get(attrName);
					JSONObject temp = new JSONObject();
					if (attrs.isBool(attrName)) {
						retObj.put(attrName, attrs.getBool(attrName));
						//System.out.println(attrName + ": " + attrs.getBool(attrName));
					} else {
						retObj.put(attrName, attrs.get(attrName));
						//System.out.println(attrName + ": " + attrs.get(attrName));
					}
					attrArr.put(temp);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retObj;
	}

	public void isDeployed(){
		isInPlay = true;
		dirty = true;
		if (attrs.get("contract") > 0) {
			owner.debt += attrs.get("contract");
		}
		// TODO: clean this up
		/*if (passiveAbilities != null) {
			for (PassiveAbility ability : passiveAbilities) {
				ability.doInitAction();
			}
		}*/
	}
	
	public void isDestroyed() {
		isInPlay = false;
		dirty = true;
		//owner.gameState.removeEventAbilities(this);
	}
	
	public enum CardType {
		UNIT, MECH, BUILDING
	}
	
	public static String cardTypeToString(CardType type) {
		switch (type) {
		case UNIT:
			return "unit";
		case MECH:
			return "mech";
		case BUILDING:
			return "building";
		default:
			return "";
		}
	}
	
	public enum Faction {
		OLD_GUARD, ANARCHIST_YOUTH, NEUTRAL, PROFESSIONALS, CLOCKWORK_MONKS, LANDLOCKED_PIRATES, NONE
	}
	
	public static String FactionToString(Faction faction) {
		switch(faction) {
		case OLD_GUARD:
			return "Old Guard";
		case ANARCHIST_YOUTH:
			return "Anarchist Youth";
		case NEUTRAL:
			return "Neutral";
		case PROFESSIONALS:
			return "Professionals";
		case NONE:
			return "";
		default:
			return "";			
		}
	}
}
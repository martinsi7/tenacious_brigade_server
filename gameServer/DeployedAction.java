package gameServer;

public interface DeployedAction {
	public void doDeployedAction(CardInfo card);
	public void UndoDeployedAction();
}

package gameServer;

import gameServer.UnitCard.UnitType;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Player implements Unit{
	public boolean 						dirty;
	public String 						name = "";
	public int 							bank = 10;
	public int 							health = 20;
	public int 							attack = 0;
	public int							debt = 0;
	public int							maxCredits = 100;
	public int 							maxCreditsPerTurn = 100;
	public Deck 						deck;
	public GameState 					gameState;
	public ArrayList<CardInfo> 			hand;
	public ArrayList<CardInfo> 			mech;
	public ArrayList<CardInfo> 			left;
	public ArrayList<CardInfo> 			center;
	public ArrayList<CardInfo> 			right;
	public ArrayList<CardInfo>			clinic;
	public boolean						isDefeated;
	//public ActionList					beginningActions;
	//public ActionList					endActions;
	//public ArrayList<TempAction>		tempBeginningActions;
	//public ArrayList<TempAction>		tempEndActions;
	public ArrayList<DeployedAction> 	deployedActions;
	
	//public AttributeHash				attrs;
	
	
	public Player(String deckString, GameState game){
		hand = new ArrayList<CardInfo>();
		mech = new ArrayList<CardInfo>();
		left = new ArrayList<CardInfo>();
		center = new ArrayList<CardInfo>();
		right = new ArrayList<CardInfo>();
		clinic = new ArrayList<CardInfo>();
		//beginningActions = new ActionList();
		//endActions = new ActionList();
		//tempBeginningActions = new ArrayList<TempAction>();
		//tempEndActions = new ArrayList<TempAction>();
		deployedActions = new ArrayList<DeployedAction>();
		dirty = true;
		deck = new Deck(deckString, this);
		gameState = game;
		isDefeated = false;
		deck.shuffle();
		
		//attrs = new AttributeHash(this);
		//attrs.set("attack", 0);
		//attrs.set("health", 20);
	}
	
	public Player(String[]deckArr, GameState game) {
		hand = new ArrayList<CardInfo>();
		mech = new ArrayList<CardInfo>();
		left = new ArrayList<CardInfo>();
		center = new ArrayList<CardInfo>();
		right = new ArrayList<CardInfo>();
		clinic = new ArrayList<CardInfo>();
		//beginningActions = new ActionList();
		//endActions = new ActionList();
		//tempBeginningActions = new ArrayList<TempAction>();
		//tempEndActions = new ArrayList<TempAction>();
		deployedActions = new ArrayList<DeployedAction>();
		dirty = true;
		deck = new Deck(deckArr, this);
		gameState = game;
		isDefeated = false;
		deck.shuffle();
		
		//attrs = new AttributeHash();
		//attrs.set("attack", 0);
		//attrs.set("health", 20);
	}
	
	public JSONObject serialize(){
		JSONObject retObj = new JSONObject();
		try {
			retObj.putOpt("name", name)
				  .putOpt("bank", new  Integer(bank))
				  .putOpt("life", health)
				  .putOpt("attack", attack)
				  .putOpt("debt", debt)
				  .putOpt("isDefeated", isDefeated)
				  .putOpt("building", deck.building.serialize(false, false, false))
				  .putOpt("canAttack", canAttack());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retObj;
	}
	
	public void drawCard(){
		CardInfo card = deck.drawCard();
		if (card != null) {
			card.zone = GameState.Zone.HAND;
			hand.add(card);
		}
	}
	
	public void earnCredits(int turn) {
		int earned = turn * 10;
		if (earned > maxCreditsPerTurn) {
			earned = maxCreditsPerTurn;
		}
		bank += earned;
		if (bank > maxCredits) {
			bank = maxCredits;
		}
		dirty = true;
	}
	
	public void playCard(GameState.Zone zone, String cardID) {
		playCard(zone, cardID, -1);
	}
	
	public void playCard(GameState.Zone zone, String cardID, int zoneIndex) {
		CardInfo card = gameState.allCards.get(cardID);
		if (card.owner != this) {
			System.err.println("Card " + card.gameID + " could not be played because the owner is wrong");
			return;
		}
		if (card.attrs.get("cost") > bank) {
			card.dirty = true;
			System.err.println("Card " + card.gameID + " could not be played because it costs too much");
			return;
		}
		// TODO check for correct placement zone with creature or mech
		ArrayList<CardInfo> newPlacement = getZoneCards(zone);
		//if (newPlacement == null) {
		//	return;
		//}
		if (zoneIndex == -1 || zoneIndex >= newPlacement.size()) {
			newPlacement.add(card);
		} else {
			newPlacement.add(zoneIndex, card);
		}
		hand.remove(card);
		bank -= card.attrs.get("cost");
		updateCardZone(card, zone);
		card.dirty = true;
		dirty = true;
		card.isInPlay = true;
		
		gameState.cardsInPlay.put(card.gameID, card);
		card.isDeployed();
		for (DeployedAction action : deployedActions) {
			action.doDeployedAction(card);
		}
	}
	
	private void updateCardZone(CardInfo card, GameState.Zone newZone) {
		ArrayList<CardInfo> temp;
		temp = getZoneArr(card.zone);
		if (temp != null) {
			temp.remove(card);
		}
		temp = getZoneArr(newZone);
		if (temp != null) {
			temp.add(card);
		}
		card.zone = newZone;
	}
	
	private ArrayList<CardInfo> getZoneArr(GameState.Zone zone) {
		switch(zone) {
		case LEFT:
			return left;
		case BUILDING:
			return null;
		case CENTER:
			return center;
		case CLINIC:
			return clinic;
		case DECK:
			return null;
		case HAND:
			return hand;
		case MECH:
			return mech;
		case NONE:
			return null;
		case RIGHT:
			return right;
		default:
			return null;
		}
	}
	
	public ArrayList<CardInfo> getZoneCards(GameState.Zone zone){
		switch(zone) {
		case CENTER:
			return orderUnitsFighterFirst(center);
		case CLINIC:
			return clinic;
		case HAND:
			return hand;
		case LEFT:
			return orderUnitsFighterFirst(left);
		case MECH:
			return mech;
		case RIGHT:
			return orderUnitsFighterFirst(right);
		case DECK:
			return deck.cards;
		case BUILDING:
			ArrayList<CardInfo> temp = new ArrayList<CardInfo>();
			temp.add(deck.building);
			return temp;
		case NONE:
		default:
			return null;
		}
	}
	
	public ArrayList<UnitCard> getUnitsInEventOrder() {
		ArrayList<UnitCard> retArr = new ArrayList<UnitCard>();
		for (CardInfo card : orderUnitsFighterFirst(left)) {
			if (card instanceof UnitCard) {
				retArr.add((UnitCard) card);
			}
		}
		for (CardInfo card : orderUnitsFighterFirst(center)) {
			if (card instanceof UnitCard) {
				retArr.add((UnitCard) card);
			}
		}
		for (CardInfo card : orderUnitsFighterFirst(right)) {
			if (card instanceof UnitCard) {
				retArr.add((UnitCard) card);
			}
		}
		return retArr;
	}
	
	public ArrayList<UnitCard> getMostMultipleUnit(String attr, boolean most) {
		return getMostMultipleUnit(attr, most, UnitType.NONE);
	}
	
	public ArrayList<UnitCard> getMostMultipleUnit(String attr, boolean most, UnitCard.UnitType type) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		int largest = (most) ? 0 : 1000;
		for (UnitCard unit : getUnitsInEventOrder()) {
			if (!unit.hasAttr(attr)) {
				continue;
			}
			if (most && unit.getAttr(attr) > largest ||
				!most && unit.getAttr(attr) < largest) {
				largest = unit.getAttr(attr);
			}
		}
		for (UnitCard unit : getUnitsInEventOrder()) {
			if (unit.getAttr(attr) == largest) {
				units.add(unit);
			}
		}
		return units;
	}
	
	public UnitCard getMostUnit(String attr, boolean most) {
		return getMostUnit(attr, most, UnitType.NONE);
	}
	
	public UnitCard getMostUnit(String attr, boolean most, UnitCard.UnitType type) {
		UnitCard target = null;
		int largest = (most) ? 0 : 1000;
		for (UnitCard unit : getUnitsInEventOrder()) {
			if (!unit.hasAttr(attr)) {
				continue;
			}
			if (most && unit.getAttr(attr) > largest ||
				!most && unit.getAttr(attr) < largest) {
				largest = unit.getAttr(attr);
				target = unit;
			}
		}
		return target;
	}
	
	public ArrayList<CardInfo> getCardsInEventOrder() {
		ArrayList<CardInfo> retArr = new ArrayList<CardInfo>();
		retArr.addAll(getZoneCards(GameState.Zone.LEFT));
		retArr.addAll(getZoneCards(GameState.Zone.CENTER));
		retArr.addAll(getZoneCards(GameState.Zone.RIGHT));
		retArr.addAll(getZoneCards(GameState.Zone.MECH));
		retArr.addAll(getZoneCards(GameState.Zone.BUILDING));
		retArr.addAll(getZoneCards(GameState.Zone.CLINIC));
		retArr.addAll(getZoneCards(GameState.Zone.HAND));
		return retArr;
	}
	
	private ArrayList<CardInfo> orderUnitsFighterFirst(ArrayList<CardInfo> cards) {
		ArrayList<CardInfo> retArr = new ArrayList<CardInfo>();
		for (CardInfo card : cards) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.unitType == UnitCard.UnitType.FIGHTER) {
					retArr.add(card);
				}
			}
		}
		for (CardInfo card : cards) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.unitType == UnitCard.UnitType.SPECIALIST) {
					retArr.add(card);
				}
			}
		}
		for (CardInfo card : cards) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.unitType == UnitCard.UnitType.NONE) {
					retArr.add(card);
				}
			} else {
				retArr.add(card);
			}
		}
		return retArr;
	}
	
	public void payDebt() {
		if (bank >= debt) {
			bank -= debt;
			debt = 0;
		} else {
			debt -= bank;
			bank = 0;
		}
		dirty = true;
	}
	
	/*public void removeEventAbilites(CardInfo card) {
		beginningActions.removeAllCardActions(card);
		endActions.removeAllCardActions(card);
	}*/
	
	public JSONArray getValidAttackers(){
		JSONArray arr = new JSONArray();
		for (CardInfo card : gameState.cardsInPlay.values()) {
			if (card.owner != this || (!(card instanceof UnitCard))) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			if (unit.canAttack()) {
				try {
					arr.put(arr.length(), unit.gameID);
				} catch (JSONException e) {
					System.err.println("unit could not be added to valid attackers: " 
							+ unit.gameID + " | " + unit.name);
					e.printStackTrace();
				}
			}
		}
		return arr;
	}
	
	public boolean canAttack() {
		if (attack > 0) {
			return true;
		}
		for (CardInfo card : gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard && card.owner == this) {
				UnitCard unit = (UnitCard) card;
				if (unit.canAttack()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public JSONObject getAllValidDefenders() {
		JSONObject obj = new JSONObject();
		System.out.println("attacking cards: " + gameState.attackingCards.size());
		for (String gameID : gameState.attackingCards) {
			System.out.println(gameID);
			GameState.Zone zone = getAttackerZone(gameID);
			ArrayList<String> defenderIDs = new ArrayList<String>();
			if (zone != GameState.Zone.NONE) {
				defenderIDs = getValidDefenders(zone);
			} else {
				System.out.println("++++++++++++++++++++++++++++++++++");
				System.out.println("no zone");
			}
			JSONArray innerArr = new JSONArray();
			for (String id : defenderIDs) {
				innerArr.put(id);
			}
			try {
				obj.put(gameID, innerArr);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return obj;
	}
	
	private ArrayList<String> getValidDefenders(GameState.Zone zone) {
		ArrayList<String> defenderIDs = new ArrayList<String>();
		for (CardInfo card : gameState.cardsInPlay.values()) {
			System.out.println("++++++++++++++++++++++++++++++++++");
			System.out.println("card in play");
			System.out.println(card.name);
			if (card.owner != this) {
				System.out.println("wrong owner");
				continue;
			}
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.canDefend(zone)) {
					defenderIDs.add(unit.gameID);
				}
			} else {
				System.out.println("not unit card");
			}
		}
		return defenderIDs;
	}
	
	private GameState.Zone getAttackerZone(String AttackerID) {
		GameState.Zone zone = GameState.Zone.NONE;
		if (AttackerID.toLowerCase().contains("player") &&
				AttackerID.split("_").length == 2) {
			zone = GameState.stringToZone(AttackerID.split("_")[1]);
		} else {
			CardInfo card = gameState.cardsInPlay.get(AttackerID);
			if (card != null) {
				zone = card.zone;
			}
		}
		return zone;
	}
	
	public void changeLane(String cardID, String newZone) {
		changeLane(cardID, newZone, -1);
	}
	
	public void changeLane(String cardID, String newZone, int zoneIndex) {
		GameState.Zone zone = GameState.stringToZone(newZone);
		CardInfo card = gameState.cardsInPlay.get(cardID);
		if (card == null || (!(card instanceof UnitCard)) || card.owner != this) {
			return;
		}
		UnitCard unit = (UnitCard) card;
		ArrayList<CardInfo> newPlacement = getZoneCards(zone);
		ArrayList<CardInfo> oldPlacement = getZoneCards(unit.zone);
		if (newPlacement == null || oldPlacement == null) {
			return;
		}
		if (zoneIndex == -1 || zoneIndex >= newPlacement.size()) {
			newPlacement.add(card);
		} else {
			newPlacement.add(zoneIndex, card);
		}
		oldPlacement.remove(card);
		if (!GameState.isAdjacentZone(card.zone, zone)) {
			return;
		}
		
		if (unit.attrs.getBool("mobility") && !unit.hasMoved && !unit.isCooldown) {
			unit.hasMoved = true;
			unit.zone = zone;
			unit.dirty = true;
		}
	}
	
	public void activateBuilding() {
		BuildingCard building = deck.building;
		if (building.attrs.get("cost") <= bank) {
			bank -= building.attrs.get("cost");
			building.isDeployed = true;
			dirty = true;
			building.isDeployed();
		}
	}

	@Override
	public void getAttacked(int amount, int snipe) {
		takeDamage(amount);
	}

	@Override
	public void takeDamage(int amount) {
		health -= amount;
		dirty = true;
		if (health <= 0) {
			isDefeated = true;
		}
		// TODO add loss condition
	}

	@Override
	public int getAttack() {
		return attack;
	}

	@Override
	public String getType() {
		return "Player";
	}

	@Override
	public boolean doesGiveDebt() {
		return false;
	}

	@Override
	public int getDebtAmount() {
		return 0;
	}

	@Override
	public void receiveDebt(int amount) {
	}
	
	@Override
	public boolean isMechanized(){
		return true;
	}

	@Override
	public int getSnipeAmount() {
		return 0;
	}
}

package gameServer;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Controller {
	public boolean hasPlayerOne = false;
	public boolean hasPlayerTwo = false;
	
	public static boolean hasPhaseChange;
	public String cleanPlayer = "";
	public String deckOne = "";
	public String deckTwo = "";
	
	public String[] deckOneArr;
	public String[] deckTwoArr;
	
	public Controller(){
		
	}
	
	public JSONObject control (JSONObject params, GameState gameState){
		JSONObject output = new JSONObject();
		try {
			output.put("result", "fail");
			switch (params.optString("action")){
			// TODO: deprecated by sending in decklists
			case "beginGame":
				beginGame(output, params, gameState);
				break;
			case "startGame":
				startGame(output, params, gameState);
				break;
			case "checkGameFull":
				checkGameFull(output, params, gameState);
				break;
			case "playCard":
				playCard(output, params, gameState);
				break;
			// TODO: this should be legacy action to be removed
			case "nextPhase":
				nextPhase(output, params, gameState);
				break;
			case "attack":
				attack(output, params, gameState);
				break;
			case "endTurn":
				endTurn(output, params, gameState);
				break;
			case "setAttackers":
				setAttackers(output, params, gameState);
				break;
			case "combat":
				combat(output, params, gameState);
				break;
			case "activateAbility":
				activateAbility(output, params, gameState);
				break;
			case "selectTargets":
				selectTargets(output, params, gameState);
				break;
			case "payDebt":
				payDebt(output, params, gameState);
				break;
			case "changeLane":
				changeLane(output, params, gameState);
				break;
			case "activateBuilding":
				activateBuilding(output, params, gameState);
				break;
			case "checkForChanges":
				checkForChanges(output, params, gameState);
				break;
			default:
				System.err.println("action not recognized");
				output.put("result", "fail");
				output.put("message", "action was not a recognized action");
			}
			return output;
		} catch (JSONException e) {
			System.err.println(e.getMessage());
			return output;
		}
	}
	
	private void beginGame(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		// TODO: dynamically add decks here
		if (params.getString("player").equals("both")) {
			hasPlayerOne = true;
			hasPlayerTwo = true;
			deckOne = params.getString("deckNameOne");
			deckTwo = params.getString("deckNameTwo");
			gameState.init(deckOne,  deckTwo);
			output.put("result", "success")
				  .put("data", gameState.serializeDirtyAndClean());
			return;
		}
		if (hasPlayerOne == hasPlayerTwo ||
				(hasPlayerOne && params.getString("player").equals("one")) ||
				(hasPlayerTwo && params.getString("player").equals("two"))) {
			if (params.getString("player").equals("one")) {
				deckOne = params.getString("deckName");
				hasPlayerOne = true;
				hasPlayerTwo = false;
			} else {
				deckTwo = params.getString("deckName");
				hasPlayerOne = false;
				hasPlayerTwo = true;
			}
			output.put("result", "success")
				  .put("isWaiting", true);
			gameState.hasChanges = false;
			return;
		} else {
			if (params.getString("player").equals("one")) {
				deckOne = params.getString("deckName");
				hasPlayerOne = true;
			} else {
				deckTwo = params.getString("deckName");
				hasPlayerTwo = true;
			}
			gameState.init(deckOne,  deckTwo);
			//gameState.init("The Ole Switcheroo",  "The Ole Switcheroo");
			output.put("result", "success")
			      .put("data", gameState.serializeDirty());
			return;
		}
	}
	
	private void startGame(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		if (params.getString("player").equals("both")) {
			gameState.init("The Ole Switcheroo",  "The Ole Switcheroo");
			output.put("result", "success")
		      .put("data", gameState.serializeDirtyAndClean());
		return;
		}
		if (hasPlayerOne == hasPlayerTwo ||
				(hasPlayerOne && params.getString("player").equals("one")) ||
				(hasPlayerTwo && params.getString("player").equals("two"))) {
			if (params.getString("player").equals("one")) {
				deckOneArr = params.getJSONArray("cards").join("::::").replaceAll("\"", "").split("::::");
				hasPlayerOne = true;
				hasPlayerTwo = false;
			} else {
				deckTwoArr = params.getJSONArray("cards").join("::::").replaceAll("\"", "").split("::::");
				hasPlayerOne = false;
				hasPlayerTwo = true;
			}
			output.put("result", "success")
				  .put("isWaiting", true);
			gameState.hasChanges = false;
			return;
		} else {
			if (params.getString("player").equals("one")) {
				deckOneArr = params.getJSONArray("cards").join("::::").replaceAll("\"", "").split("::::");
				hasPlayerOne = true;
			} else {
				deckTwoArr = params.getJSONArray("cards").join("::::").replaceAll("\"", "").split("::::");
				hasPlayerTwo = true;
			}
			gameState.init(deckOneArr,  deckTwoArr);
			//gameState.init("The Ole Switcheroo",  "The Ole Switcheroo");
			output.put("result", "success")
			      .put("data", gameState.serializeDirty());
			return;
		}
	}
	
	private void checkGameFull(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		if (hasPlayerOne && hasPlayerTwo) {
			output.put("ready", true)
				  .put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("ready", false);
		}
		output.put("result", "success");
	}
	
	private void playCard(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		String cardID = params.getString("cardID");
		GameState.Zone zone = GameState.stringToZone(params.getString("zone"));
		int index = params.getInt("index");
		gameState.curPlayer.playCard(zone, cardID, index);
		output.put("result", "success");
		if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}
		cleanPlayer = params.getString("player");
	}
	
	//TODO: this should be legacy code
	private void nextPhase(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		gameState.nextPhase();
		getPhaseChangeInfo(output, params, gameState);
		cleanPlayer = params.getString("player");
	}
	
	private void attack(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		if (gameState.curPhase == GameState.Phase.FIRSTMAIN){
			nextPhase(output, params, gameState);
		}
	}
	
	private void endTurn(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		System.out.println("called end turn");
		if (params.get("player").equals(gameState.curPlayer.name) || params.get("player").equals("both")) {
			System.out.println("if true");
			gameState.changeTurn();
			getPhaseChangeInfo(output, params, gameState);
			cleanPlayer = params.getString("player");
			System.out.println(output.toString());
			output.put("result", "success");
		}
	}
	
	private void setAttackers(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		if (params.optJSONArray("attackers") == null){
			output.put("result", "success");
			if (params.getString("player").equals("both")) {
				output.put("data", gameState.serializeDirtyAndClean());
			} else {
				output.put("data", gameState.serializeDirty());
			}
			gameState.nextPhase();
			gameState.nextPhase();
			getPhaseChangeInfo(output, params, gameState);
			return;
		}
		gameState.setAttackers(params.optJSONArray("attackers"));
		output.put("result", "success");
		/*if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}*/
		getPhaseChangeInfo(output, params, gameState);
		cleanPlayer = params.getString("player");
	}
	
	private void combat(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		JSONArray combats = params.optJSONArray("combats");
		if (combats == null){
			output.put("result", "success");
			
			/*if (params.getString("player").equals("both")) {
				output.put("data", gameState.serializeDirtyAndClean());
			} else {
				output.put("data", gameState.serializeDirty());
			}*/
			return;
		}
		gameState.resolveAllCombat(combats);
		output.put("result", "success");
		/*if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}*/
		getPhaseChangeInfo(output, params, gameState);
		cleanPlayer = params.getString("player");
	}
	
	private void activateAbility(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		String ownerCardID = params.getString("cardID");
		int abilityIndex = params.getInt("index");
		JSONObject targetInfo = gameState.activateAbility(ownerCardID, abilityIndex);
		output.put("result", "success")
			  .put("targetInfo", targetInfo);
		if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}
		cleanPlayer = params.getString("player");
	}
	
	private void selectTargets(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		TargetInfo targets = getTargetInfo(params.getJSONObject("targetInfo"), gameState);
		gameState.doResolvedAction(targets);
		output.put("result", "success");
		if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}
		cleanPlayer = params.getString("player");
	}
	
	private void payDebt (JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		gameState.curPlayer.payDebt();
		output.put("result", "success");
		if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}
		cleanPlayer = params.getString("player");
	}
	
	private void changeLane(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		String cardID1 = params.getString("cardID");
		String newZone = params.getString("newLane");
		gameState.curPlayer.changeLane(cardID1, newZone);
		output.put("result", "success");
		if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}
		cleanPlayer = params.getString("player");
	}
	
	private void activateBuilding(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		gameState.curPlayer.activateBuilding();
		output.put("result", "success");
		if (params.getString("player").equals("both")) {
			output.put("data", gameState.serializeDirtyAndClean());
		} else {
			output.put("data", gameState.serializeDirty());
		}
		cleanPlayer = params.getString("player");
	}
	
	private void getPhaseChangeInfo(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		JSONObject data = null;
		if (params.getString("player").equals("both")) {
			data = gameState.serializeDirtyAndClean();
		} else {
			data = gameState.serializeDirty();
		}
		data.put("phase", GameState.prettifyPhaseName(gameState.curPhase));
		if (gameState.curPhase == GameState.Phase.DEFEND) {
			data.put("activePlayer", gameState.otherPlayer.name);
		} else {
			data.put("activePlayer", gameState.curPlayer.name);
		}
		data.put("turn", gameState.turn);
		output.put("result", "success")
			  .put("data", data);
		if (gameState.curPhase == GameState.Phase.ATTACK) {
			output.put("validAttackers", gameState.curPlayer.getValidAttackers())
					  .put("canPlayerAttack", (gameState.curPlayer.attack > 0));
		} else if (gameState.curPhase == GameState.Phase.DEFEND) {
			output.put("validCombats", gameState.otherPlayer.getAllValidDefenders());
		}
		hasPhaseChange = true;
		System.out.println("resulting output");
		System.out.println(output);
	}
	
	private void checkForChanges(JSONObject output, JSONObject params, GameState gameState) throws JSONException {
		if (gameState.hasChanges && !params.getString("player").equals(cleanPlayer)) {
			System.out.println("+++++++++++++++++++++++++++++++++++++++++");
			System.out.println("hasPhaseChange? " + hasPhaseChange);
			if (hasPhaseChange) {
				JSONObject data1 = gameState.serializeDirtyAndClean();
				data1.put("phase", GameState.prettifyPhaseName(gameState.curPhase));
				if (gameState.curPhase == GameState.Phase.DEFEND) {
					data1.put("activePlayer", gameState.otherPlayer.name);
				} else {
					data1.put("activePlayer", gameState.curPlayer.name);
				}
				data1.put("turn", gameState.turn);
				output.put("result", "success")
					  .put("hasChanges", true)
					  .put("data", data1);
				if (gameState.curPhase == GameState.Phase.ATTACK) {
					output.put("validAttackers", gameState.curPlayer.getValidAttackers())
							  .put("canPlayerAttack", (gameState.curPlayer.attack > 0));
				} else if (gameState.curPhase == GameState.Phase.DEFEND) {
					output.put("validCombats", gameState.otherPlayer.getAllValidDefenders());
				}
				if (gameState.showCombatChanges) {
					output.put("combatResults", gameState.combatResults);
					gameState.showCombatChanges = false;
				}
				hasPhaseChange = false;
			} else {
				output.put("result", "success")
				      .put("hasChanges", true)
		  	  	      .put("data", gameState.serializeDirtyAndClean());
			}
			
		} else {
			output.put("result", "success")
				  .put("hasChanges", false);
		}
	}
	
	private TargetInfo getTargetInfo(JSONObject obj, GameState gameState) {
		try {
			//boolean hasTarget = obj.getBoolean("hasTarget");
			boolean targetOtherPlayer = obj.getBoolean("canTargetOtherPlayer");
			boolean targetSelfPlayer = obj.getBoolean("canTargetSelfPlayer");
			//String ownerCardID = obj.getString("ownerCardID");
			String ownerCardID = gameState.waitingAction.owner.gameID;
			ArrayList<CardInfo> targets = new ArrayList<CardInfo>();
			for (int i=0; i < obj.getJSONArray("validTargets").length(); i++) {
				String targetID =  obj.getJSONArray("validTargets").getString(i);
				CardInfo card = gameState.allCards.get(targetID);
				targets.add(card);
			}
			return new TargetInfo(targets, targetOtherPlayer, targetSelfPlayer, ownerCardID);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}

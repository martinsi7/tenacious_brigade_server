package gameServer;

import java.util.ArrayList;

/*import org.json.JSONException;
import org.json.JSONObject;
*/

public abstract class MechCardInfo extends CardInfo {
	//public int attack;

	public MechCardInfo(Player player) {
		super(player);
		cardType = CardType.MECH;
	}
	
	@Override
	public void setup() {
		attrs.set("attack", 0);
		activeAbilities = new ArrayList<ActiveAbility>();
	}
	
	@Override
	public void doStartTurnAction() {
		startTurnAction();
	}
	
	public void startTurnAction(){}
	
	@Override
	public void doEndTurnAction() {
		endTurnAction();
	}
	
	public void endTurnAction(){}
	
	public void isDeployed(){
		super.isDeployed();
		//owner.attack += this.attack;
		owner.attack += getAttr("attack");
	}
	
	public void isDestroyed() {
		super.isDestroyed();
		owner.attack -= getAttr("attack");
	}
	
	/*@Override
	public JSONObject serialize(boolean isEndTurn, boolean isBeginningTurn){
		JSONObject retObj = super.serialize(isEndTurn, isBeginningTurn);
		try {
			retObj.putOpt("attack", getAttr("attack"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retObj;
	}*/
}

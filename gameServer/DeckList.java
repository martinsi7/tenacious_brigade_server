package gameServer;

import java.util.ArrayList;

import clockworkMonkCards.*;

import oldGuardCards.*;

import mechCard.*;

import card.*;

import ProfessionalsCards.*;
import anarchistYouthCards.*;
import buildings.*;
//import java.util.Arrays;



public class DeckList {
	public static ArrayList<CardInfo> constructCards(String[] cardList, Player player){
		ArrayList<CardInfo> retCards = new ArrayList<CardInfo>();
		for (int i = 0; i < cardList.length; i++){
			/*if (i == 0) {
				for (int j = 0; j < cardList[i].length(); j++) {
					System.out.println(cardList[i].charAt(j));
				}
			}*/
			CardInfo card = DeckList.getCardFromString(cardList[i], player);
			if (card == null) {
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++");
				System.out.println(cardList[i] + " is null");
				throw new Error("card does not exist" + cardList[i]);
			}
			retCards.add(card);
		}
		return retCards;
	}
	
	public static ArrayList<CardInfo> deckOne(Player player){
		String[] stringArr = {
				"Tenacious Marines",
				"Tenacious Marines",
				"Tenacious Marines",
				"Tenacious Marines",
				"Swashbuckling Matey",
				"Swashbuckling Matey",
				"Forward Battlements",
				"Field Medic",
				"Field Medic",
				"Field Medic"
				};
		return DeckList.constructCards(stringArr, player);
	}
	
	public static ArrayList<CardInfo> deckTwo(Player player){
		String[] stringArr = {
				"Tenacious Marines",
				"Assassin",
				"Assassin",
				"Helm of Awe",
				"Combat Drone",
				"Combat Drone",
				"Combat Drone",
				"Combat Drone",
				"Forward Battlements",
				"Helm of Awe"
				};
		return DeckList.constructCards(stringArr, player);
	}
	
	public static ArrayList<CardInfo> oldGuardOne(Player player) {
		String[] stringArr = {
			"Forward Battlements",
			"Reinforced Turret",
			"Stimulant Vendor",
			"Stubborn Veteran",
			"Weathered Mercenary",
			"Turreted Barricade",
			"Heavy Arms Dealer",
			"Makeshift Tank",
			"Ranged Artillery Expert",
			"Automated Defense System",
			"Force Hammer",
			"Rocket Pistol",
			"Medical Supplies",
			"Distracting Battlecry"
		};
		return DeckList.constructCards(stringArr, player);
	}
	
	public static ArrayList<CardInfo> mightOfOaks(Player player) {
		String[] stringArr = {
			"Cowardly Hero",
			"Giant Grower",
			"Behemoth Grower",
			"Quartermaster",
			"Short Fused Droid",
			"Weathered Mercenary",
			"Support Marine",
			"Makeshift Tank",
			"Homesick Soldier",
			"Military Transport",
			"Rocket Pistol",
			"Might of Explosives",
			"Helm of Cooperation",
			"Bionics Factory"
		};
		return DeckList.constructCards(stringArr, player);
	}
	
	public static ArrayList<CardInfo> timeToGoMobile(Player player) {
		String[] stringArr = {
			"Eager Rebel",
			"Hoverboard Courier",
			"Angry Junkie",
			"Lazy Looter",
			"Ignorant Brute",
			"Temperamental Drone",
			"Jetpack Courier",
			"Hotwired Junker",
			"Demolitions Expert",
			"Makeshift Ballista",
			"Volatile Flamejets",
			"Force Hammer",
			"Laser Sight",
			"Flanking Support Towers"
		};
		return DeckList.constructCards(stringArr, player);
	}
	
	public static CardInfo getCardFromString (String cardStr, Player player) {
		switch (cardStr){
		case "Helm of Awe":
			return new HelmOfAwe(player);
		case "Assassin":
			return new Assassin(player);
		case "Combat Drone":
			return new CombatDrone(player);
		case "Field Medic":
			return new FieldMedic(player);
		case "Swashbuckling Matey":
			return new SwashbucklingMatey(player);
		/********************************************************************
		 * Neutral
		 ********************************************************************/
		case "Force Hammer":
			return new ForceHammer(player);
		case "Rocket Pistol":
			return new RocketPistol(player);
		case "Whirling Mace":
			return new WhirlingMace(player);
		case "Nuclear Armament":
			return new NuclearArmament(player);
		case "Cracked Helmet":
			return new CrackedHelmet(player);
		/********************************************************************
		 * Buildings
		 ********************************************************************/
		case "Forward Battlements":
			return new ForwardBattlements(player);
		case "New Federation Tax Office":
			return new NewFederationTaxOffice(player);
		case "Flanking Support Towers":
			return new FlankingSupportTowers(player);
		case "Bionics Factory":
			return new BionicsFactory(player);
		case "Hidden Vault":
			return new HiddenVault(player);
		case "Open Armory":
			return new OpenArmory(player);
		case "Field Clinic":
			return new FieldClinic(player);
		case "Pestilence, Inc.":
			return new PestilenceInc(player);
		case "Party Central":
			return new PartyCentral(player);
		case "Grandpa No Arm's Quarters":
			return new BobsQuarters(player);
		case "Grandpa No Arm's Tent":
			return new BobsTent(player);
		case "Grandpa No Arm's Pavillion":
			return new BobsPavillion(player);
		case "Crowded Bunker":
			return new CrowdedBunker(player);
		case "Captain X's Distress Beacon":
			return new CaptainBobsDistressBeacon(player);
		/********************************************************************
		 * Old Guard
		 ********************************************************************/
		case "Tenacious Marines":
			return new TenaciousMarines(player);
		case "Reinforced Turret":
			return new ReinforcedTurret(player);
		case "Vigilant Sentry":
			return new VigilantSentry(player);
		case "Stimulant Vendor":
			return new StimulantVendor(player);
		case "Stubborn Veteran":
			return new StubbornVeteran(player);
		case "Weathered Mercenary":
			return new WeatheredMercenary(player);
		case "Turreted Barricade":
			return new TurretedBarricade(player);
		case "Heavy Arms Dealer":
			return new HeavyArmsDealer(player);
		case "Makeshift Tank":
			return new MakeshiftTank(player);
		case "Ranged Artillery Expert":
			return new RangedArtilleryExpert(player);
		case "Automated Defense System":
			return new AutomatedDefenseSystem(player);
		
		case "Medical Supplies":
			return new MedicalSupplies(player);
		case "Distracting Battlecry":
			return new DistractingBattlecry(player);
		case "Cowardly Hero":
			return new CowardlyHero(player);
		case "Giant Grower":
			return new GiantGrower(player);
		case "Behemoth Grower":
			return new BehemothGrower(player);
		case "Quartermaster":
			return new Quartermaster(player);
		case "Short Fused Droid":
			return new ShortFusedDroid(player);
		case "Support Marine":
			return new SupportMarine(player);
		case "Homesick Soldier":
			return new HomesickSoldier(player);
		case "Military Transport":
			return new MilitaryTransport(player);
		case "Might of Explosives":
			return new MightOfExplosives(player);
		case "Helm of Cooperation":
			return new HelmOfCooperation(player);
		/********************************************************************
		 * Anarchist Youth
		 ********************************************************************/
		case "Eager Rebel":
			return new EagerRebel(player);
		case "Hoverboard Courier":
			return new HoverboardCourier(player);
		case "Angry Junkie":
			return new AngryJunkie(player);
		case "Lazy Looter":
			return new LazyLooter(player);
		case "Ignorant Brute":
			return new IgnorantBrute(player);
		case "Temperamental Drone":
			return new TemperamentalDrone(player);
		case "Jetpack Courier":
			return new JetpackCourier(player);
		case "Hotwired Junker":
			return new HotwiredJunker(player);
		case "Demolitions Expert":
			return new DemolitionsExpert(player);
		case "Makeshift Ballista":
			return new MakeshiftBallista(player);
		case "Volatile Flamejets":
			return new VolatileFlamejets(player);
		case "Human Cannon":
		case "Laser Sight":
			return new HumanCannon(player);
		case "Suicidal Idiot":
			return new SuicidalIdiot(player);
		case "Pestering Slanderer":
			return new PesteringSlanderer(player);
		case "Aggravating Slanderer":
			return new AggravatingSlanderer(player);
		case "Steroid Dealer":
			return new SteroidDealer(player);
		case "Helm of the Berserker":
			return new HelmOfTheBerserker(player);
		case "Belligerent Radical":
			return new BelligerentRadical(player);
		case "Insightful Radical":
			return new InsightfulRadical(player);
		case "Steadfast Radical":
			return new SteadfastRadical(player);
		case "Enraged Radical":
			return new EnragedRadical(player);
		case "Spark of Riot":
			return new SparkOfRiot(player);
		case "Spark of Revolution":
			return new SparkOfRevolution(player);
			
		/********************************************************************
		 * Clockwork Monks
		 ********************************************************************/
		case "Serial Cannibal":
			return new SerialCannibal(player);
		case "Sleeping Blooddrinker":
			return new SleepingBlooddrinker(player);
		case "Sacrificial Lamb":
			return new SacrificialLamb(player);
		case "Destitute Ritualist":
			return new DestituteRitualist(player);
		case "Poison Thrower":
			return new PoisonThrower(player);
		case "Enhanced Ritualist":
			return new EnhancedRitualist(player);
		case "Jumble of Blades":
			return new JumbleOfBlades(player);
		case "Scrapyard Hybrid":
			return new ScrapyardHybrid(player);
		case "Shambling Deplon":
			return new ShamblingDeplon(player);
		case "Darkest Monk":
			return new DarkestMonk(player);
		case "Waster":
			return new Waster(player);
		case "Wanter":
			return new Wanter(player);
		case "Low Priest":
			return new LowPriest(player);
		case "Scholar of Gears":
			return new ScholarOfGears(player);
		case "Scholar of Spikes":
			return new ScholarOfSpikes(player);
		case "Miscellaneous Bot":
			return new MiscellaneousBot(player);
		case "Gearhead Monk":
			return new GearheadMonk(player);
		case "Desperate Exile":
			return new DesperateExile(player);
		case "Clockwork Effigy":
			return new ClockworkEffigy(player);
		case "Stim Pack":
			return new StimPack(player);
		case "Bloodpower Disk":
			return new BloodpowerDisk(player);
		case "Mechanic Converter":
			return new MechanicConverter(player);
		case "Tortured Priestess":
			return new TorturedPriestess(player);
		case "Clockwork Monkey":
			return new ClockworkMonkey(player);
		case "Sanguine Ritualist":
			return new SanguineRitualist(player);
		case "Sanguine Oathtaker":
			return new SanguineOathtaker(player);
		case "Sanguine Master":
			return new SanguineMaster(player);
		case "Frenzied Reject":
			return new FrenziedReject(player);
		case "Frantic Pariah":
			return new FranticPariah(player);
		case "Plague Carrier":
			return new PlagueCarrier(player);
		case "Priestess of the Blood Cow":
			return new PriestessOfTheBloodCow(player);
		case "Priest of the Rotting Bull":
			return new PriestOfTheRottingBull(player);
		case "Broke Steve":
			return new BrokeSteve(player);
		/********************************************************************
		 * Professionals
		 ********************************************************************/
		case "Disguised Civilian":
			return new DisguisedCivilian(player);
		case "Veteran Tactician":
			return new VeteranTactician(player);
		case "Patient Invader":
			return new PatientInvader(player);
		case "Glass Shard Slinger":
			return new GlassShardSlinger(player);
		case "Covert Companion":
			return new CovertCompanion(player);
		case "Inspiring Agent":
			return new InspiringAgent(player);
		case "Prolific Sniper":
			return new ProlificSniper(player);
		case "Camoflaged Spotter":
			return new CamoflagedSpotter(player);
		case "Rifle Cart":
			return new RifleCart(player);
		case "Roving Infiltrator":
			return new RovingInfiltrator(player);
		case "Fresh Inductee":
			return new FreshInductee(player);
		case "Steadfast Killer":
			return new SteadfastKiller(player);
		case "Trained Brute":
			return new TrainedBrute(player);
		case "Methodical Striker":
			return new MethodicalStriker(player);
		case "Reckless Operative":
			return new RecklessOperative(player);
		case "Long Scope Merc":
			return new LongScopeMerc(player);
		case "Cautious Killer":
			return new CautiousKiller(player);
		case "Mini Kill Bot":
			return new MiniKillBot(player);
		case "Invading Rifleman":
			return new InvadingRifleman(player);
		case "Local Defense Agent":
			return new LocalDefenseAgent(player);
		case "Laser Guide":
			return new LaserGuide(player);
		case "Targeted Stealth Field":
			return new TargetedStealthField(player);
		case "Small Arms Dealer":
			return new SmallArmsDealer(player);
		}
		return null;
	}
	
	public static ArrayList<CardInfo> getDeckFromString(String deckString, Player player){
		switch (deckString){
		case "Deck One":
			return deckOne(player);
		case "Deck Two":
			return deckTwo(player);
		case "The Ole Switcheroo":
			return oldGuardOne(player);
		case "Might of Oaks":
			return mightOfOaks(player);
		case "Time to Go Mobile":
			return timeToGoMobile(player);
		default:
			return deckOne(player);
		}
	}
}

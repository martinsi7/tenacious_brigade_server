package gameServer;

public abstract class PassiveAbility extends CardAbility {
	public boolean setToTemp = false;
	public boolean tempAction = false;

	public PassiveAbility(CardInfo newOwner){
		this.owner = newOwner;
		this.isTargetedAbility = false;
	}
	
	public abstract void insertIntoActionList();

}

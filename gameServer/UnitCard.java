package gameServer;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class UnitCard extends CardInfo implements Unit {
	public boolean					isPrepTime;
	public boolean					isCooldown;
	public int 						cooldown;
	public boolean					hasMoved;
	
	/*
	TODO: remove unused
	public int						health;
	public int						extortion;
	public boolean					aggressive;
	public boolean					defensive;
	public boolean					reach;
	public boolean					mobility;
	
	public boolean					tenacity;
	public boolean					regeneration;
	public boolean					shambling;
	public boolean					mechanized;
	public boolean					berserker;
	public int						snipe;
	public boolean 					infiltrate;
	*/
	public UnitType					unitType;

	public UnitCard(Player player) {
		super(player);
		isPrepTime = false;
		cardType = CardType.UNIT;
	}
	
	public static UnitCard asUnit(CardInfo card) {
		if (card instanceof UnitCard) {
			return (UnitCard) card;
		} else {
			return null;
		}
	}
	
	@Override
	public void setup() {
		attrs.set("aggressive", false);
		attrs.set("extortion", 0);
		attrs.set("defensive", false);
		attrs.set("reach", false);
		attrs.set("mobility", false);
		attrs.set("tenacity", false);
		attrs.set("shambling", false);
		attrs.set("mechanized", false);
		attrs.set("berserker", false);
		attrs.set("infiltrate", false);
		activeAbilities = new ArrayList<ActiveAbility>();
	}
	
	@Override
	public void doStartTurnAction() {
		if (!isCooldown && !isPrepTime) {
			startTurnAction();
		}
	}
	
	public void startTurnAction(){}
	
	@Override
	public void doEndTurnAction() {
		if (!isCooldown && !isPrepTime) {
			endTurnAction();
		}
	}
	
	public void endTurnAction(){}
	
	@Override
	public void isDestroyed() {
		cooldown = 0;
		isCooldown = false;
		isPrepTime = false;
		super.isDestroyed();
	}
	
	public boolean canAttack() {
		if (isPrepTime) {
			return false;
		} else if (isCooldown) {
			return false;
		} else if (attrs.getBool("defensive")) {
			return false;
		}
		return true;
	}
	
	public boolean canDefend(GameState.Zone attackerZone) {
		if (attackerZone != zone) {
			if (
				!(
					attrs.getBool("reach") &&
					GameState.isAdjacentZone(zone, attackerZone)
				)
				){
				return false;
			}
		} else if (isCooldown) {
			return false;
		} else if (attrs.getBool("aggressive")) {
			return false;
		}
		return true;
	}
	
	public void recoverCooldown() {
		hasMoved = false;
		if (isPrepTime) {
			isPrepTime = false;
			dirty = true;
		}
		if (isCooldown) {
			int amount = 1;
			if (attrs.getBool("regeneration") && cooldown >= 2) {
				amount = 2;
			}
			cooldown -= amount;
			if (cooldown <= 0) {
				cooldown = 0;
				isCooldown = false;
			}
			dirty = true;
		}
		if (attrs.getBool("berserker")) {
			attrs.set("attack", attrs.get("attack") + 1);
			attrs.set("health", attrs.get("health") - 1);
			if (attrs.get("health") <= 0) {
				goToClinic();
			}
			attrs.updateStatus("attack");
			attrs.updateStatus("health");
			dirty = true;
		}
	}
	
	@Override
	public void isDeployed(){
		isPrepTime = true;
		super.isDeployed();
	}

	@Override
	public void getAttacked(int amount, int snipeAmount) {
		if (attrs.getBool("shambling")) {
			takeDamage(amount);
			amount = 0;
		}
		int damageThrough = amount - attrs.get("health");
		if (attrs.getBool("tenacity") && owner == owner.gameState.otherPlayer) {
			if (amount % 2 == 1) {
				amount++;
			}
			damageThrough = (amount - attrs.get("health")) / 2;
		}
		damageThrough = Math.max(damageThrough, snipeAmount);
		if (damageThrough > 0){
			isCooldown = true;
			cooldown = damageThrough;
			dirty = true;
		}
	}
	
	@Override
	public void takeDamage(int amount) {
		if (amount > 0) {
			dirty = true;
			attrs.change("health", -1 * amount);
			if (attrs.get("health") <= 0){
				goToClinic();
			}
		}
	}
	
	@Override
	public int getAttack(){
		return attrs.get("attack");
	}
	
	@Override
	public String getType(){
		return "Card";
	}
	
	public boolean isValidAction(){
		if (isCooldown){
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public JSONObject serialize(boolean isEndTurn, boolean isBeginningTurn, boolean hide){
		JSONObject retObj = super.serialize(isEndTurn, isBeginningTurn, hide);
		hide = isHidden();
		if (hide) {
			return retObj;
		}
		try {
			// TODO: delete unused
			retObj.putOpt("cooldown", new Integer(cooldown))
				  //.putOpt("health", new Integer(health))
				  //.putOpt("attack", new Integer(attack))
				  .putOpt("isPrepTime", isPrepTime)
				  //.putOpt("mobility", mobility)
				  .putOpt("canMove", canMove())
				  .putOpt("unitType", unitTypeToString(unitType))
				  //.putOpt("extortion", extortion)
				  //.putOpt("aggressive", aggressive)
				  //.putOpt("defensive", defensive)
				  //.putOpt("reach", reach)
				  //.putOpt("tenacity", tenacity)
				  //.putOpt("regeneration", regeneration)
				  //.putOpt("shambling", shambling)
				  //.putOpt("mechanized", mechanized)
				  ;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retObj;
	}
	
	public boolean canMove(){
		return !hasMoved &&
				attrs.getBool("mobility") &&
				(cooldown == 0) &&
				(isPrepTime == false) &&
				(owner == owner.gameState.curPlayer);
	}
	
	@Override
	public boolean doesGiveDebt() {
		return (attrs.get("extortion") > 0);
	}

	@Override
	public int getDebtAmount() {
		return attrs.get("extortion");
	}
	
	@Override
	public void receiveDebt(int amount) {
		owner.debt += amount * 10;
		owner.dirty = true;
	}
	
	@Override
	public boolean isMechanized(){
		return attrs.getBool("mechanized");
	}
	
	@Override
	public int getSnipeAmount() {
		return attrs.get("snipe");
	}
	
	public enum UnitType {
		FIGHTER, SPECIALIST, NONE
	}
	
	public static String unitTypeToString(UnitType type) {
		switch (type) {
		case FIGHTER:
			return "fighter";
		case SPECIALIST:
			return "specialist";
		case NONE:
			return "none";
		default:
			return "";
				
		}
	}
}

package gameServer;

import java.util.ArrayList;

public class AbilityTargetterHelper {
	
	public static ArrayList<UnitCard> allOwnedTargets(GameState state, CardAbility ability) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			Player player = ability.owner.owner;
			if (player != card.owner) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			units.add(unit);
		}
		return units;
	}
	
	public static ArrayList<UnitCard> allEnemyTargets(GameState state, CardAbility ability) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			Player player = ability.owner.owner;
			if (player == card.owner) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			units.add(unit);
		}
		return units;
	}
	
	public static ArrayList<UnitCard> allOwnedTargetsOnCooldown(GameState state, CardAbility ability) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			Player player = ability.owner.owner;
			if (player != card.owner) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			if (unit.isCooldown) {
				units.add(unit);
			}
		}
		return units;
	}
	
	public static ArrayList<UnitCard> sameLaneTargets(GameState state, CardAbility ability) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			Player player = ability.owner.owner;
			if (player != card.owner || card.zone != ability.owner.zone) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			units.add(unit);
		}
		return units;
	}
	
	public static ArrayList<UnitCard> sameLaneTargetsOnCooldown(GameState state, CardAbility ability) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			Player player = ability.owner.owner;
			if (player != card.owner || card.zone != ability.owner.zone) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			if (unit.isCooldown) {
				units.add(unit);
			}
		}
		return units;
	}
	
	public static ArrayList<UnitCard> oppositeLaneTargets(GameState state, CardAbility ability) {
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			Player player = ability.owner.owner;
			if (player == card.owner || card.zone != ability.owner.zone) {
				continue;
			}
			UnitCard unit = (UnitCard) card;
			units.add(unit);
		}
		return units;
	}
	
	public static ArrayList<CardInfo> unitCardsToCardInfo(ArrayList<UnitCard> unitCards) {
		ArrayList<CardInfo> cards = new ArrayList<CardInfo>();
		for (UnitCard unit : unitCards) {
			cards.add((CardInfo) unit);
		}
		return cards;
	}

}

package gameServer;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {
	public ArrayList<CardInfo> cards;
	public BuildingCard building;
	public static final int MAX_BUILDING_PROGRESS = 10;
	public Player owner;
	
	public Deck(String deckString, Player player){
		owner = player;
		cards = DeckList.getDeckFromString(deckString, player);
		for (int i = 0; i < cards.size(); i++) {
			CardInfo card = cards.get(i);
			if (card instanceof BuildingCard) {
				building = (BuildingCard) card;
				cards.remove(i);
				break;
			}
		}
		this.shuffle();
	}
	
	public Deck(String[] deckArr, Player player) {
		owner = player;
		cards = DeckList.constructCards(deckArr, player);
		for (int i = 0; i < cards.size(); i++) {
			CardInfo card = cards.get(i);
			if (card instanceof BuildingCard) {
				building = (BuildingCard) card;
				cards.remove(i);
				break;
			}
		}
		this.shuffle();
	}
	
	public CardInfo drawCard(){
		if (cards.size() == 0){
			return null;
		}
		CardInfo card = cards.get(0);
		card.dirty = true;
		cards.remove(0);
		return card;
	}
	
	public void shuffle(){
		Collections.shuffle(cards);
	}
	
	public void incrementBuildingProgress() {
		if (!building.isReady) {
			building.progress++;
			owner.dirty = true;
		}
		if (building.progress == MAX_BUILDING_PROGRESS) {
			building.isReady = true;
		}
	}
}

package gameServer;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class BuildingCard extends CardInfo {
	public int progress = 0;
	public boolean isReady = false;
	public boolean isDeployed = false;

	public BuildingCard(Player player) {
		super(player);
		cardType = CardType.BUILDING;
		zone = GameState.Zone.BUILDING;
		faction = Faction.NEUTRAL;
	}
	
	@Override
	public void doStartTurnAction() {
		if (isDeployed) {
			startTurnAction();
		}
	}
	
	public void startTurnAction(){}
	
	@Override
	public void doEndTurnAction() {
		if (isDeployed) {
			endTurnAction();
		}
	}
	
	public void endTurnAction(){}
	
	public void isDeployed(){}
	
	@Override
	public JSONObject serialize(boolean isEndTurn, boolean isBeginningTurn, boolean hide){
		JSONObject retObj = super.serialize(isEndTurn, isBeginningTurn, false);
		try {
			retObj.put("progress", progress)
				  .put("ready", isReady)
				  .put("deployed", isDeployed);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return retObj;
	}

}

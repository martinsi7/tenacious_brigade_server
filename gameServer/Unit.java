package gameServer;

public interface Unit {
	public int getAttack();
	public String getType();
	public void getAttacked(int amount, int snipe);
	public void takeDamage(int amount);
	public boolean doesGiveDebt();
	public int getDebtAmount();
	public void receiveDebt(int amount);
	public boolean isMechanized();
	public int getSnipeAmount();
}

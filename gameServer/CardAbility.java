package gameServer;

public abstract class CardAbility {
	public CardInfo owner;
	public boolean isTargetedAbility;
	public int numTargets;
	
	
	// returns null if isTargetedAbility == false
	public abstract TargetInfo doInitAction();
	
	// if isTargetedAbility == false returns null
	// else adds this to gameState.waitingAction
	public abstract TargetInfo doEventAction();
	
	public abstract void doResolvedAction(TargetInfo targetInfo);
	
	public abstract TargetInfo getTargets();
}

package gameServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class GameServer {
	private static final Logger log = Logger.getLogger( "Application" );

	/**
	 * @param args
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public static void main(String[] args) throws IOException, JSONException {
		FileHandler fileHandler = new FileHandler("log.xml");
		log.addHandler(fileHandler);
		ServerSocket listener = new ServerSocket(25001);
		GameState gameState = null;
		Controller controller = new Controller();
        try {
            while (true) {
                Socket socket = listener.accept();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                try {
                     char[] buf = new char[2048];
                     int inputChars = -1;
                     //ParamHash inputParams = new ParamHash();
                     inputChars = in.read(buf);
                     if (inputChars == 0){
                    	 out.write("{\"fail\"}{\"nothing read\"}");
                    	 out.flush();
                     }
                     String bufString = new String(buf).replace("\u0000", "");
                     JSONObject obj = new JSONObject(bufString);
                     System.out.println("***********************");
                     System.out.println(obj.toString());
                     System.out.println("***********************");
                     if (obj.optString("action").equals("beginGame") || obj.optString("action").equals("startGame")) {
                    	 if (obj.optString("player").equals("both") ||
                    			 controller.hasPlayerOne == controller.hasPlayerTwo) {
                    		 gameState = new GameState();
                    	 }
                     }
                     gameState.requester = obj.getString("player");
                     JSONObject output = controller.control(obj, gameState);
                     System.out.println("******************* output ***********************");
                     System.out.println(output.toString());
     				 System.out.println("*******************        ***********************");
                     out.write(output.toString());
                	 out.flush();
                } catch (JSONException e){
                	System.err.println(e.getMessage());
                	 out.write("error");
                	 out.flush();
                	 throw e;
                } catch (Exception ex) {
                	log.log( Level.SEVERE, ex.toString(), ex );
                	throw ex;
                	//continue;
                	
                } finally {
                	socket.close();
                	in.close();
                	out.close();
                }
            }
        }
        finally {
        	
            listener.close();
        }
	}
}

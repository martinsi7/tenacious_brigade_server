package gameServer;

public abstract class ActiveAbility extends CardAbility {
	public int cost;
	public String description;
	public boolean hasCooldownCost;
	public int lifeCost;
	
	public ActiveAbility(int newCost, CardInfo newOwner, int numOfTargets, boolean cooldownCost){
		this.cost = newCost;
		this.owner = newOwner;
		this.isTargetedAbility = false;
		description = "";
		numTargets = numOfTargets;
		hasCooldownCost = cooldownCost;
	}
	
	public boolean canAfford() {
		if (cost > owner.owner.bank){
			return false;
		} else {
			return true;
		}
	}
	
	public void pay() {
		owner.owner.bank -= cost;
		if (lifeCost > 0) {
			owner.owner.health -= lifeCost;
		}
		owner.owner.dirty = true;
		if (hasCooldownCost && (owner instanceof UnitCard)) {
			UnitCard unit = (UnitCard) owner;
			unit.cooldown += 1;
			unit.isCooldown = true;
			unit.dirty = true;
		}
		System.out.println("new bank: " + owner.owner.bank);
		
	}

}

package ProfessionalsCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneInfiltrate;
import activeAbilities.TargetChangeCooldown;

public class CovertCompanion extends UnitCard {

	public CovertCompanion(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Covert Companion";
		description = "";
		attrs.set("attack", 0);
		attrs.set("health", 1);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		SameLaneInfiltrate targetter = new SameLaneInfiltrate();
		TargetChangeCooldown ability = new TargetChangeCooldown(targetter, 10, this, 1, 1, true);
		ability.description = "$10, cooldown: target unit in this lane with infiltrate has its cooldown reduced by 1";
		
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

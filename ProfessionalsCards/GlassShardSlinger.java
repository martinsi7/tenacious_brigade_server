package ProfessionalsCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import activeAbilities.Infiltrate;

public class GlassShardSlinger extends UnitCard {

	public GlassShardSlinger(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Glass Shard Slinger";
		description = "";
		attrs.set("attack", 3);
		attrs.set("health", 2);
		attrs.set("cost", 30);
		attrs.setTrue("shambling");
		unitType = UnitType.SPECIALIST;
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		Infiltrate ability = new Infiltrate(0, this, 1, true);
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

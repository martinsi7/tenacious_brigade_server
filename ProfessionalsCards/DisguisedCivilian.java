package ProfessionalsCards;

import java.util.ArrayList;

import activeAbilities.Infiltrate;
import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

public class DisguisedCivilian extends UnitCard {

	public DisguisedCivilian(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Disguised Civilian";
		description = "infiltrate: 2";
		attrs.set("attack", 2);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		unitType = UnitType.SPECIALIST;
		attrs.set("infiltrate", 2);
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		Infiltrate ability = new Infiltrate(30, this, 1, true);
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

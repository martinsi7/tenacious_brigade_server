package ProfessionalsCards;

import gameServer.MechCardInfo;
import gameServer.Player;

public class TargetedStealthField extends MechCardInfo {

	public TargetedStealthField(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		name = "Targeted Stealth Field";
		description = "At the beginning of your turn, " +
				"the 2 friendly Professional fighters with the highest attack gain" +
				"Infiltrate: 1 until end of turn";
		attrs.set("cost", 30);
		attrs.set("attack", 0);
		//setPassiveAbilities();
	}
	
	/*private void setPassiveAbilities(){
		TwoHighestAttackFighters targetter = new TwoHighestAttackFighters(Faction.PROFESSIONALS);
		ChangeAbilitiesTemp ability = new ChangeAbilitiesTemp(this, targetter, "infiltrate", false, true, 1);
		passiveAbilities = new ArrayList<PassiveAbility>();
		passiveAbilities.add(ability);
	}*/

}

package ProfessionalsCards;

import java.util.ArrayList;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.OppositeLaneTargetter;
import activeAbilities.TargetChangeCooldown;

public class ProlificSniper extends UnitCard {

	public ProlificSniper(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Prolific Sniper";
		description = "";
		attrs.set("attack", 3);
		attrs.set("health", 2);
		attrs.set("cost", 40);
		unitType = UnitType.SPECIALIST;
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		OppositeLaneTargetter targetter = new OppositeLaneTargetter();
		TargetChangeCooldown ability = new TargetChangeCooldown(targetter, 0, this, 1, 2, true);
		ability.description = "$0, cooldown: 2 target units in the opposing lane recieve cooldown 1";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package ProfessionalsCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class MethodicalStriker extends UnitCard {

	public MethodicalStriker(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Methodical Striker";
		description = "contract 10<br/>Deployment: if this is the " +
				"only fighter in this lane it gains mobility";
		attrs.set("attack", 5);
		attrs.set("health", 5);
		attrs.set("cost", 70);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 10;
		owner.dirty = true;
		boolean isOnlyFighterInLane = true;
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.zone == zone && unit.unitType == UnitType.FIGHTER) {
					isOnlyFighterInLane = false;
				}
			}
		}
		if (isOnlyFighterInLane){
			attrs.setTrue("mobility");
			dirty = true;
		}
	}

}

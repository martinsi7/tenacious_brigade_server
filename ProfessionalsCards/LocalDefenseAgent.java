package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class LocalDefenseAgent extends UnitCard {
	
	public LocalDefenseAgent(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}
	

	@Override
	public void setup() {
		super.setup();
		name = "Local Defense Agent";
		description = "Defensive. Snipe 1";
		attrs.set("attack", 2);
		attrs.set("health", 4);
		attrs.set("cost", 20);
		attrs.setTrue("defensive");
		attrs.set("snipe", 1);
		unitType = UnitType.FIGHTER;
	}

}

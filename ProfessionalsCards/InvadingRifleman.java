package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class InvadingRifleman extends UnitCard {

	public InvadingRifleman(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Invading Rifleman";
		description = "aggressive. snipe 1";
		attrs.set("attack", 2);
		attrs.set("health", 2);
		attrs.set("cost", 10);
		attrs.setTrue("aggressive");
		attrs.set("snipe", 1);
		unitType = UnitType.FIGHTER;
	}

}

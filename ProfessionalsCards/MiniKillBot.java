package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class MiniKillBot extends UnitCard {

	public MiniKillBot(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Mini Kill Bot";
		description = "tenacity";
		attrs.set("attack", 2);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		attrs.setTrue("tenacity");
		unitType = UnitType.FIGHTER;
	}

}

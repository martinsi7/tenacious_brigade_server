package ProfessionalsCards;

import gameServer.MechCardInfo;
import gameServer.Player;

public class LaserGuide extends MechCardInfo {

	public LaserGuide(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		name = "Laser Guide";
		description = "At the beginning of your turn, the friendly Professional specialist" +
				" with the highest attack gains snipe +2";
		attrs.set("cost", 40);
		attrs.set("attack", 1);
		//setPassiveAbilities();
	}
	
	/*private void setPassiveAbilities(){
		HighestAttackSpecialist targetter = new HighestAttackSpecialist(Faction.PROFESSIONALS);
		ChangeAbilitiesTemp ability = new ChangeAbilitiesTemp(this, targetter, "snipe", true, false, 2);
		passiveAbilities = new ArrayList<PassiveAbility>();
		passiveAbilities.add(ability);
	}*/

}

package ProfessionalsCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class RifleCart extends UnitCard {

	public RifleCart(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Rifle Cart";
		description = "Deployment: give all specialists in this lane snipe 2";
		attrs.set("attack", 0);
		attrs.set("health", 4);
		attrs.set("cost", 40);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		for(CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.zone == zone && unit.owner == owner &&
						unit.unitType == UnitType.SPECIALIST) {
					unit.attrs.addChanger("snipe", this, false, false, 2);
					unit.dirty = true;
				}
			}
		}
	}

}

package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class FreshInductee extends UnitCard {

	public FreshInductee(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Fresh Inductee";
		description = "snipe 2";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 20);
		attrs.set("snipe", 2);
		unitType = UnitType.FIGHTER;
	}

}

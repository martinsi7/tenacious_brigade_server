package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class RovingInfiltrator extends UnitCard {

	public RovingInfiltrator(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Roving Infiltrator";
		description = "Mobility";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 50);
		attrs.setTrue("mobility");
		attrs.set("infiltrate", 2);
		unitType = UnitType.SPECIALIST;
	}
}

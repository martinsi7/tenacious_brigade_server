package ProfessionalsCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import activeAbilities.TargetMultipleAbilities;

public class CamoflagedSpotter extends UnitCard {

	public CamoflagedSpotter(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Camoflaged Spotter";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		SameLaneTargetter targetter = new SameLaneTargetter();
		String[] attrs = {"health", "snipe"};
		int[] amounts = {1, 2};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(0, this, 1, targetter, true, attrs, amounts, true, false);
		ability.description = "$0, Cooldown: Give target unit in this lane +0/+1 and Snipe 2 " +
				"until the beginning of your next turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class PatientInvader extends UnitCard {

	public PatientInvader(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Patient Invader";
		description = "";
		attrs.set("attack", 2);
		attrs.set("health", 2);
		attrs.set("cost", 50);
		attrs.set("infiltrate", 4);
		unitType = UnitType.SPECIALIST;
	}

}

package ProfessionalsCards;

import gameServer.GameState.Zone;
import gameServer.Player;
import gameServer.UnitCard;

public class RecklessOperative extends UnitCard {
	
	public RecklessOperative(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Reckless Operative";
		description = "Snipe 2<br/>If this unit deals combat damage, " +
				"put it in the clinic at end of combat";
		attrs.set("attack", 4);
		attrs.set("health", 4);
		attrs.set("cost", 30);
		attrs.set("snipe", 2);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void recoverCooldown() {
		super.recoverCooldown();
		zone = Zone.CLINIC;
		dirty = true;
	}
	
	@Override
	public void takeDamage(int amount) {
		super.takeDamage(amount);
		zone = Zone.CLINIC;
		dirty = true;
	}

}

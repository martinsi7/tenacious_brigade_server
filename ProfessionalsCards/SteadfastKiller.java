package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class SteadfastKiller extends UnitCard {

	public SteadfastKiller(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Steadfast Killer";
		description = "snipe 4";
		attrs.set("attack", 1);
		attrs.set("health", 4);
		attrs.set("cost", 50);
		attrs.set("snipe", 4);
		unitType = UnitType.FIGHTER;
	}

}

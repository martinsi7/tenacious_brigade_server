package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class TrainedBrute extends UnitCard {

	public TrainedBrute(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Trained Brute";
		description = "contract 20";
		attrs.set("attack", 5);
		attrs.set("health", 5);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 20;
		owner.dirty = true;
	}

}

package ProfessionalsCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneFighter;
import activeAbilities.TargetChangeAbilities;

public class VeteranTactician extends UnitCard {

	public VeteranTactician(Player player) {
		super(player);
	}

	@Override
	public void setup() {
		super.setup();
		name = "Veteran Tactician";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		SameLaneFighter targetter = new SameLaneFighter();
		TargetChangeAbilities ability = new TargetChangeAbilities(0, this, 1, targetter, "snipe", true, false, true);
		ability.description = "$0, cooldown: target fighter in this lane gets snipe 2 until end of turn";
		activeAbilities = new ArrayList<ActiveAbility>();
		activeAbilities.add(ability);
	}

}

package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.SameLaneSpecialist;
import activeAbilities.TargetChangeAbilities;
import cardStatus.StatusChanger;

public class InspiringAgent extends UnitCard {

	public InspiringAgent(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Inspiring Agent";
		description = "";
		attrs.set("attack", 3);
		attrs.set("health", 2);
		attrs.set("cost", 40);
		unitType = UnitType.SPECIALIST;
		setupActiveAbilities();
	}
	
	private void setupActiveAbilities(){
		StatusChanger changer = new StatusChanger(this, false, true, 1);
		SameLaneSpecialist targetter = new SameLaneSpecialist();
		TargetChangeAbilities ability = new TargetChangeAbilities(20, this, 2, targetter, "attack", changer, false);
		ability.description = "$20: 2 target units in this lane gain +1/+0 until end of turn";
		activeAbilities.add(ability);
	}

}

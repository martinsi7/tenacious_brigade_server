package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class LongScopeMerc extends UnitCard {

	public LongScopeMerc(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Long Scope Merc";
		description = "contract 20. Reach";
		attrs.set("attack", 2);
		attrs.set("health", 3);
		attrs.set("cost", 20);
		attrs.setTrue("reach");
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		owner.debt += 20;
		owner.dirty = true;
	}

}

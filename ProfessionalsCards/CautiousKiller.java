package ProfessionalsCards;

import gameServer.Player;
import gameServer.UnitCard;

public class CautiousKiller extends UnitCard {

	public CautiousKiller(Player player) {
		super(player);
		faction = Faction.PROFESSIONALS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Cautious Killer";
		description = "At the beginning of your turn, gain " +
				"snipe 2 until end of turn";
		attrs.set("attack", 1);
		attrs.set("health", 4);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
		setupPassiveAbilities();
	}
	
	private void setupPassiveAbilities() {
		//SelfTargetter targetter = new SelfTargetter();
		//ChangeAbilitiesTemp ability = new ChangeAbilitiesTemp(this, targetter, "snipe", false, true);
		//passiveAbilities = new ArrayList<PassiveAbility>();
		//passiveAbilities.add(ability);
	}

}

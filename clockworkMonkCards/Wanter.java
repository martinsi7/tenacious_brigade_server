package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class Wanter extends UnitCard {

	public Wanter(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Wanter";
		description = "Shambling.<br/>At the beginning of your turn," +
				" restore this unit to full health.";
		attrs.set("attack", 5);
		attrs.set("health", 6);
		attrs.set("cost", 50);
		attrs.set("shambling", true);
		unitType = UnitType.FIGHTER;
	}
	
	// TODO: should I instead keep track of combat damage and only restore that?
	//		 card ability would read differently
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		if (getAttr("health") < 6) {
			int diff = 6 - getAttr("health");
			attrs.addChanger("health", null, false, false, diff);
		}
	}

}

package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class FrenziedReject extends UnitCard {

	public FrenziedReject(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Frenzied Reject";
		description = "Deployment: If you have 10 or less life, gain +3/+3.";
		attrs.set("attack", 3);
		attrs.set("health", 3);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		if (owner.health <= 10) {
			attrs.set("attack", 6);
			attrs.set("health", 6);
		}
	}

}

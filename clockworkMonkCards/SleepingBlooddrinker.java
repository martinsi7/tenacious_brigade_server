package clockworkMonkCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.AbilityTargetter;
import abilityTargetter.SelfTargetter;
import activeAbilities.TargetChangeAbilities;

public class SleepingBlooddrinker extends UnitCard {

	public SleepingBlooddrinker(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Sleeping Blooddrinker";
		description = "Defensive.";
		attrs.set("attack", 2);
		attrs.set("health", 2);
		attrs.set("cost", 10);
		attrs.set("defensive", true);
		unitType = UnitType.FIGHTER;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AbilityTargetter targetter = new SelfTargetter();
		ActiveAbility ability = new TargetChangeAbilities(0, this, 1, targetter, "defensive", 0, false, false, true);
		ability.lifeCost = 1;
		ability.description = "Pay 1 Life: Loses Defensive until end of turn";
		activeAbilities.add(ability);
	}

}

package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class TorturedPriestess extends UnitCard {

	public TorturedPriestess(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Tortured Priestess";
		description = "Shambling<br/>At the beginning of your turn, " +
				"this unit loses 1 health and all fighters in this lane gain +2/+0 " +
				"until the end of your turn";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 20);
		attrs.set("shambling", true);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit != null && unit.unitType == UnitType.FIGHTER) {
				unit.attrs.addChanger("attack", this, true, false, 2);
			}
		}
	}

}

package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class DesperateExile extends UnitCard {

	public DesperateExile(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Desperate Exile";
		description = "Shambling<br/>" +
				"Deployment: If you have 15 or less life, gain +2/+3.";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 20);
		attrs.set("shambling", true);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		if (owner.health <= 15) {
			attrs.set("attack", 3);
			attrs.set("health", 4);
		}
	}

}

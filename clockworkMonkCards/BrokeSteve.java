package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class BrokeSteve extends UnitCard {

	public BrokeSteve(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Darkest Monk";
		description = "nuthin.";
		attrs.set("attack", 3);
		attrs.set("health", 3);
		attrs.set("cost", 30);
		unitType = UnitType.FIGHTER;
	}

}

package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.TargetInfo;
import gameServer.UnitCard;
import abilityTargetter.AllOwnedTargetter;
import activeAbilities.CustomAbility;

public class StimPack extends MechCardInfo {

	public StimPack(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Stim Pack";
		description = "";
		attrs.set("attack", 2);
		attrs.set("cost", 60);
		setActiveAbilities();
	}
	
	@Override
	public void customAbility(TargetInfo targetInfo) {
		int numFighters = 0;
		for (UnitCard unit : owner.getUnitsInEventOrder()) {
			if (unit.unitType == UnitCard.UnitType.FIGHTER) {
				numFighters++;
				if (unit.faction == Faction.CLOCKWORK_MONKS) {
					unit.attrs.addChanger("attack", this, false, false, 2);
					unit.attrs.addChanger("health", this, false, false, 1);
				}
			}
		}
		owner.takeDamage(numFighters);
	}
	
	private void setActiveAbilities() {
		AllOwnedTargetter targetter = new AllOwnedTargetter(CardInfo.Faction.CLOCKWORK_MONKS, "", UnitCard.UnitType.FIGHTER);
		CustomAbility ability = new CustomAbility(50, this, 1, false, targetter);
		ability.description = "50: each clockwork monk fighter you control gains +2/+1. you lose 1 life for each fighter you control.";
		activeAbilities.add(ability);
	}

}

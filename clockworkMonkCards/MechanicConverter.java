package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.AllOwnedTargetter;
import activeAbilities.TargetMultipleAbilities;

public class MechanicConverter extends MechCardInfo {

	public MechanicConverter(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Mechanic Converter";
		description = "";
		attrs.set("attack", 1);
		attrs.set("cost", 50);
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AllOwnedTargetter targetter = new AllOwnedTargetter(CardInfo.Faction.CLOCKWORK_MONKS, "", UnitCard.UnitType.FIGHTER);
		String[] _attrs = {"attack", "health", "mechanized"};
		int[] amounts = {1, 1};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(20, this, 1, targetter, false, _attrs, amounts, false, false);
		ability.description = "20: target clockwork monk specialist gains +1/+1 and mechanized until your next turn";
		activeAbilities.add(ability);
	}

}

package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class EnhancedRitualist extends UnitCard {

	public EnhancedRitualist(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Enhanced Ritualist";
		description = "Mechanized.";
		attrs.set("attack", 3);
		attrs.set("health", 3);
		attrs.set("cost", 40);
		attrs.set("mechanized", true);
		unitType = UnitType.FIGHTER;
	}

}

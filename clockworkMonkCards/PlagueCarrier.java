package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.TargetInfo;
import gameServer.UnitCard;
import abilityTargetter.AbilityTargetter;
import abilityTargetter.SelfTargetter;
import activeAbilities.CustomAbility;

public class PlagueCarrier extends UnitCard {

	public PlagueCarrier(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Plague Carrier";
		description = "Defensive.";
		attrs.set("attack", 0);
		attrs.set("health", 4);
		attrs.set("cost", 40);
		unitType = UnitType.FIGHTER;
		setActiveAbilities();
	}
	
	@Override
	public void customAbility(TargetInfo targetInfo) {
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit != null && unit.unitType == UnitType.FIGHTER) {
				unit.attrs.addChanger("attack", this, false, false, -2);
				unit.attrs.addChanger("health", this, false, false, -2);
			}
		}
		for (CardInfo card : owner.gameState.otherPlayer.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit != null && unit.unitType == UnitType.FIGHTER) {
				unit.attrs.addChanger("attack", this, false, false, -2);
				unit.attrs.addChanger("health", this, false, false, -2);
			}
		}
		goToClinic();
	}
	
	private void setActiveAbilities() {
		AbilityTargetter targetter = new SelfTargetter();
		CustomAbility ability = new CustomAbility(0, this, 1, false, targetter);
		ability.description = "send this unit to the clinic: all fighters in this and opposing lane gain -2/-2";
		activeAbilities.add(ability);
	}

}

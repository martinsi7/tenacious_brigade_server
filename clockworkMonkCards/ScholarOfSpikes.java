package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.SameLaneFighter;
import activeAbilities.TargetChangeAbilities;

public class ScholarOfSpikes extends UnitCard {

	public ScholarOfSpikes(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Scholar of Spikes";
		description = "";
		attrs.set("attack", 2);
		attrs.set("health", 2);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneFighter targetter = new SameLaneFighter();
		TargetChangeAbilities ability = new TargetChangeAbilities(0, this, 1, targetter, "mechanized", true, true, false);
		ability.lifeCost = 1;
		ability.description = "Cooldown, Pay 1 Life: Give target friendly fighter in this lane Mechanized until your next turn";
		activeAbilities.add(ability);
	}

}

package clockworkMonkCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import activeAbilities.Sacrifice;

public class SacrificialLamb extends UnitCard {

	public SacrificialLamb(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Sacrificial Lamb";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		activeAbilities = new ArrayList<ActiveAbility>();
		String[] attrs = {"attack", "health"};
		int[] amounts = {4, 4};
		ActiveAbility ability = new Sacrifice(0, this, 1, false, attrs, amounts, true, false);
		ability.description = "Send this unit to the Clinic: Give target friendly unit in this lane +4/+4 until your next turn.";
		activeAbilities.add(ability);
	}

}

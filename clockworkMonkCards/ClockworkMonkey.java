package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class ClockworkMonkey extends UnitCard {

	public ClockworkMonkey(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Clockwork Monkey";
		description = "Deployment: each player takes the highest health unit in " +
				"this lane that he or she controls and puts it in the " +
				"clinic (if one or more units are tied, they all go to the clinic)";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 60);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		doDeplyAbility(owner);
		doDeplyAbility(owner.gameState.otherPlayer);
	}
	
	private void doDeplyAbility(Player player) {
		int highestHealth = 0;
		for (CardInfo card : player.getZoneCards(this.zone)) {
			if (card.getAttr("health") > highestHealth) {
				highestHealth = card.getAttr("health");
			}
		}
		for (CardInfo card : player.getZoneCards(this.zone)) {
			if (card.getAttr("health") == highestHealth) {
				card.goToClinic();
			}
		}
	}

}

package clockworkMonkCards;

import gameServer.ActiveAbility;
import gameServer.GameState;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.AbilityTargetter;
import abilityTargetter.SelfTargetter;
import activeAbilities.TargetChangeAbilities;

public class JumbleOfBlades extends UnitCard {

	public JumbleOfBlades(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Jumble of Blades";
		description = "Shambling. When this unit takes combat damage, it loses all abilities.";
		attrs.set("attack", 2);
		attrs.set("health", 5);
		attrs.set("cost", 30);
		attrs.set("shambling", true);
		unitType = UnitType.FIGHTER;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AbilityTargetter targetter = new SelfTargetter();
		ActiveAbility ability = new TargetChangeAbilities(0, this, 1, targetter, "attack", 1, false, false, false);
		ability.lifeCost = 2;
		ability.description = "Pay 2 Life: gain +1 attack.";
		activeAbilities.add(ability);
	}
	
	@Override
	public void takeDamage(int amount) {
		super.takeDamage(amount);
		if (amount > 0 && this.zone != GameState.Zone.CLINIC) {
			attrs.set("shambling", false);
			activeAbilities = new ArrayList<ActiveAbility>();
		}
	}

}

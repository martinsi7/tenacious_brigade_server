package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class Waster extends UnitCard {

	public Waster(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Waster";
		description = "Mechanized<br/>When this unit is put " +
				"on cooldown from combat, it loses 1 health " +
				" for each turn of Cooldown it is on";
		attrs.set("attack", 5);
		attrs.set("health", 2);
		attrs.set("cost", 50);
		attrs.set("mechanized", true);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void getAttacked(int amount, int snipeAmount) {
		super.getAttacked(amount, snipeAmount);
		this.attrs.change("health", -1 * this.cooldown);
	}
	
}

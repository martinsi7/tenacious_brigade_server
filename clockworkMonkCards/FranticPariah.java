package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class FranticPariah extends UnitCard {

	public FranticPariah(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Frantic Pariah";
		description = "Deployment: If you have 10 or less life, gain +4/+4.";
		attrs.set("attack", 5);
		attrs.set("health", 5);
		attrs.set("cost", 60);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		if (owner.health <= 5) {
			attrs.set("attack", 9);
			attrs.set("health", 9);
		}
	}

}

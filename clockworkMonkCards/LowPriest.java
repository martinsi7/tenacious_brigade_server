package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class LowPriest extends UnitCard {

	public LowPriest(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Low Priest";
		description = "Shambling, Mechanized.<br/>" +
				"Deployment: Gain +1 Attack for each attack point of your mech.";
		attrs.set("attack", 1);
		attrs.set("health", 7);
		attrs.set("cost", 60);
		attrs.set("mechanized", true);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		int attack = owner.getAttack();
		if (attack > 0) {
			attrs.set("attack", attrs.get("attack") + attack);
		}
	}

}

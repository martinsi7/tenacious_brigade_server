package clockworkMonkCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;

import java.util.ArrayList;

import activeAbilities.Cannibalize;

public class SerialCannibal extends UnitCard {

	public SerialCannibal(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Serial Cannibal";
		description = "";
		attrs.set("attack", 1);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		activeAbilities = new ArrayList<ActiveAbility>();
		ActiveAbility ability = new Cannibalize(0, this, 1, false);
		ability.description = "Send a friendly unit in this lane to clinic: +3/+3 and mobility until end of turn";
		activeAbilities.add(ability);
	}

}

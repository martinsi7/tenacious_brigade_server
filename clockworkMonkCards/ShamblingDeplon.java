package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class ShamblingDeplon extends UnitCard {

	public ShamblingDeplon(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Sleeping Blooddrinker";
		description = "Shambling.<br/>Deployment: Your highest health unit"+
						" in this lane goes into the clinic and this unit"+
						" gains +X/+X where X is equal to the health of the unit";
		attrs.set("attack", 3);
		attrs.set("health", 4);
		attrs.set("cost", 50);
		attrs.set("shambling", true);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		int highestHealth = 0;
		UnitCard target = null;
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			if (card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.getAttr("health") > highestHealth) {
					target = unit;
					highestHealth = unit.getAttr("health");
				}
			}
		}
		if (target != null) {
			attrs.set("attack", attrs.get("attack") + highestHealth);
			attrs.set("health", attrs.get("health") + highestHealth);
			target.goToClinic();
		}
	}

}

package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class ClockworkEffigy extends UnitCard {

	public ClockworkEffigy(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Clockwork Effigy";
		description = "Shambling, Reach.<br/>" +
				"Deployment: Gains +1 Health for each life point you have";
		attrs.set("attack", 0);
		attrs.set("health", 1);
		attrs.set("cost", 80);
		attrs.set("shambling", true);
		attrs.set("reach", true);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void isDeployed() {
		super.isDeployed();
		attrs.set("health", owner.health + 1);
	}
	
}

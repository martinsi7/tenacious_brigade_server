package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class PoisonThrower extends MechCardInfo {

	public PoisonThrower(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		name = "Poison Thrower";
		description = "At the beginning of your turn, if you do not have a " +
				"Clockwork Monks unit in play, gain debt 10";
		attrs.set("cost", 10);
		attrs.set("attack", 1);
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		for (CardInfo card : owner.gameState.cardsInPlay.values()) {
			if (card != this && card instanceof UnitCard) {
				UnitCard unit = (UnitCard) card;
				if (unit.owner == owner && unit.faction == UnitCard.Faction.CLOCKWORK_MONKS) {
					return;
				}
			}
		}
		owner.debt += 10;
		owner.dirty = true;
	}

}

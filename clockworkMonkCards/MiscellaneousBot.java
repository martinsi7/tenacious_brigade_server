package clockworkMonkCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.AbilityTargetter;
import abilityTargetter.SelfTargetter;
import activeAbilities.TargetChangeAbilities;
import activeAbilities.TargetMultipleAbilities;

public class MiscellaneousBot extends UnitCard {

	public MiscellaneousBot(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Miscellaneous Bot";
		description = "";
		attrs.set("attack", 3);
		attrs.set("health", 3);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AbilityTargetter targetter = new SelfTargetter();
		ActiveAbility ability = new TargetChangeAbilities(10, this, 1, targetter, "mechanized", false, true, false);
		ability.description = "10: Gain Mechanized until your next turn";
		activeAbilities.add(ability);
		ability = new TargetChangeAbilities(10, this, 1, targetter, "tenacity", false, true, false);
		ability.description = "10: Gain Tenacity until your next turn";
		activeAbilities.add(ability);
		ability = new TargetChangeAbilities(10, this, 1, targetter, "mobility", false, true, false);
		ability.description = "10: Gain Mobility until your next turn";
		activeAbilities.add(ability);
		
		String[] _attrs = {"attack", "health"};
		int[] amounts = {1, -1};
		ability = new TargetMultipleAbilities(10, this, 1, targetter, false, _attrs, amounts, true, false);
		ability.description = "10: Gain +1/-1 until your next turn";
		activeAbilities.add(ability);
		
		int[] amounts2 = {-1, 1};
		ability = new TargetMultipleAbilities(10, this, 1, targetter, false, _attrs, amounts2, true, false);
		ability.description = "10: Gain -1/+1 until your next turn";
		activeAbilities.add(ability);
	}

}

package clockworkMonkCards;

import gameServer.ActiveAbility;
import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.AbilityTargetter;
import abilityTargetter.SelfTargetter;
import activeAbilities.TargetChangeAbilities;

public class DarkestMonk extends UnitCard {

	public DarkestMonk(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Darkest Monk";
		description = "Mechanized.";
		attrs.set("attack", 1);
		attrs.set("health", 3);
		attrs.set("cost", 30);
		attrs.set("mechanized", true);
		unitType = UnitType.FIGHTER;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		AbilityTargetter targetter = new SelfTargetter();
		ActiveAbility ability = new TargetChangeAbilities(0, this, 1, targetter, "attack", 2, false, false, true);
		ability.lifeCost = 3;
		ability.description = "Pay 3 Life: Gain +2 attack until end of turn";
		activeAbilities.add(ability);
	}

}

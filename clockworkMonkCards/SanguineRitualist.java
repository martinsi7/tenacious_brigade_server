package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class SanguineRitualist extends UnitCard {

	public SanguineRitualist(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Sanguine Ritualist";
		description = "contract 10.<br/>At the beginning of your turn, " +
				"send this unit to the clinic and give the friendly " +
				"fighter in this lane with the lowest health +2/+2";
		attrs.set("attack", 1);
		attrs.set("health", 4);
		attrs.set("cost", 10);
		attrs.set("contract", 10);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		int lowestHealth = 1000;
		UnitCard target = null;
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit != null && unit.unitType == UnitType.FIGHTER) {
				if (unit.getAttr("health") < lowestHealth) {
					lowestHealth = unit.getAttr("health");
					target = unit;
				}
			}
		}
		if (target != null) {
			target.attrs.addChanger("attack", this, false, false, 2);
			target.attrs.addChanger("health", this, false, false, 2);
		}
		goToClinic();
	}

}

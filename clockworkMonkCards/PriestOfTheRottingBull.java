package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class PriestOfTheRottingBull extends UnitCard {

	public PriestOfTheRottingBull(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Priest of the Rotting Bull";
		description = "At the beginning of your turn, " +
				"the highest attack unit in this lane gains +1/+0 " +
				"for all units in this lane with mechanized until " +
				"the beginning of your next turn.";
		attrs.set("attack", 1);
		attrs.set("health", 2);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		UnitCard target = null;
		int highestHealth = 0;
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit == null || unit.isCooldown) {
				continue;
			}
			if (unit.getAttr("attack") > highestHealth) {
				target = unit;
				highestHealth = unit.getAttr("attack");
			}
		}
		if (target != null) {
			int numMech = countMechanized(owner);
			target.attrs.addChanger("attack", this, true, false, numMech);
		}
	}
	
	private int countMechanized(Player player) {
		int count = 0;
		for (CardInfo card : player.getZoneCards(this.zone)) {
			if (card.getBool("mechanized")) {
				count++;
			}
		}
		return count;
	}

}

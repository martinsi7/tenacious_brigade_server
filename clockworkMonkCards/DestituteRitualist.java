package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class DestituteRitualist extends UnitCard {

	public DestituteRitualist(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Destitute Ritualist";
		description = "Shambling.";
		attrs.set("attack", 0);
		attrs.set("health", 1);
		attrs.set("cost", 10);
		attrs.set("shambling", true);
		unitType = UnitType.FIGHTER;
	}

}

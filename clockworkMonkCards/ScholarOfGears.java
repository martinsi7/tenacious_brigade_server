package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.SameLaneFighter;
import activeAbilities.TargetMultipleAbilities;

public class ScholarOfGears extends UnitCard {

	public ScholarOfGears(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Scholar of Gears";
		description = "";
		attrs.set("attack", 0);
		attrs.set("health", 3);
		attrs.set("cost", 20);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneFighter targetter = new SameLaneFighter();
		String[] _attrs = {"health", "tenacity"};
		int[] amounts = {2};
		TargetMultipleAbilities ability = new TargetMultipleAbilities(0, this, 1, targetter, true, _attrs, amounts, true, false);
		ability.lifeCost = 1;
		ability.description = "Cooldown, Pay 1 Life: " +
				"Give target friendly fighter in this lane +0/+2 and Tenacity until your next turn";
		activeAbilities.add(ability);
	}

}

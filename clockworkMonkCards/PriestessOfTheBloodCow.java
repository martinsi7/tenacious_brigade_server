package clockworkMonkCards;

import gameServer.CardInfo;
import gameServer.Player;
import gameServer.UnitCard;

public class PriestessOfTheBloodCow extends UnitCard {

	public PriestessOfTheBloodCow(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Priestess of the Blood Cow";
		description = "At the beginning of your turn, " +
				"the highest health unit in this lane gains +0/+1 " +
				"for all units in this and the opposing lane with " +
				"shambling until the beginning of your next turn.";
		attrs.set("attack", 0);
		attrs.set("health", 4);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
	}
	
	@Override
	public void startTurnAction() {
		super.startTurnAction();
		UnitCard target = null;
		int highestHealth = 0;
		for (CardInfo card : owner.getZoneCards(this.zone)) {
			UnitCard unit = asUnit(card);
			if (unit == null || unit.isCooldown) {
				continue;
			}
			if (unit.getAttr("health") > highestHealth) {
				target = unit;
				highestHealth = unit.getAttr("health");
			}
		}
		if (target != null) {
			int numShambling = countShambling(owner) + countShambling(owner.gameState.otherPlayer);
			target.attrs.addChanger("health", this, true, false, numShambling);
		}
	}
	
	private int countShambling(Player player) {
		int count = 0;
		for (CardInfo card : player.getZoneCards(this.zone)) {
			if (card.getBool("shambling")) {
				count++;
			}
		}
		return count;
	}

}

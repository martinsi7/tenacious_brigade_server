package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;

public class ScrapyardHybrid extends UnitCard {

	public ScrapyardHybrid(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}

	@Override
	public void setup() {
		super.setup();
		name = "Scrapyard Hybrid";
		description = "Shambling. When this unit is put into the clinic reduce its cost by 30.";
		attrs.set("attack", 4);
		attrs.set("health", 2);
		attrs.set("cost", 40);
		attrs.set("defensive", true);
		unitType = UnitType.FIGHTER;
	}
	
	@Override
	public void goToClinic() {
		super.goToClinic();
		attrs.set("cost", attrs.get("cost") - 30);
	}

}

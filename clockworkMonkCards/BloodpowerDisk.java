package clockworkMonkCards;

import abilityTargetter.AllOwnedTargetter;
import activeAbilities.CustomAbility;
import gameServer.CardInfo;
import gameServer.MechCardInfo;
import gameServer.Player;
import gameServer.TargetInfo;
import gameServer.UnitCard;

public class BloodpowerDisk extends MechCardInfo {

	public BloodpowerDisk(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Bloodpower Disk";
		description = "";
		attrs.set("attack", 0);
		attrs.set("cost", 40);
		setActiveAbilities();
	}
	
	@Override
	public void customAbility(TargetInfo targetInfo) {
		int numFighters = 0;
		for (UnitCard unit : owner.getUnitsInEventOrder()) {
			if (unit.unitType == UnitCard.UnitType.FIGHTER) {
				if (unit.faction == Faction.CLOCKWORK_MONKS) {
					unit.goToClinic();
					numFighters++;
				}
			}
		}
		owner.gameState.otherPlayer.takeDamage(numFighters);
	}
	
	private void setActiveAbilities() {
		AllOwnedTargetter targetter = new AllOwnedTargetter(CardInfo.Faction.CLOCKWORK_MONKS, "", UnitCard.UnitType.FIGHTER);
		CustomAbility ability = new CustomAbility(100, this, 1, false, targetter);
		ability.description = "100: put all friendly clockwork monk fighters into the clinic and deal that much damage to your opponent.";
		activeAbilities.add(ability);
	}

}

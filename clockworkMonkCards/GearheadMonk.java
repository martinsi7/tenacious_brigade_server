package clockworkMonkCards;

import gameServer.Player;
import gameServer.UnitCard;
import abilityTargetter.SameLaneWithAttr;
import activeAbilities.TargetChangeAbilities;

public class GearheadMonk extends UnitCard {

	public GearheadMonk(Player player) {
		super(player);
		faction = Faction.CLOCKWORK_MONKS;
	}
	
	@Override
	public void setup() {
		super.setup();
		name = "Gearhead Monk";
		description = "";
		attrs.set("attack", 0);
		attrs.set("health", 3);
		attrs.set("cost", 30);
		unitType = UnitType.SPECIALIST;
		setActiveAbilities();
	}
	
	private void setActiveAbilities() {
		SameLaneWithAttr targetter = new SameLaneWithAttr("shambling");
		TargetChangeAbilities ability = new TargetChangeAbilities(0, this, 1, targetter, "shambling", 3, true, false, false);
		ability.description = "Cooldown: Target friendly unit in this lane with Shambling gains +0/+3";
		activeAbilities.add(ability);
	}

}

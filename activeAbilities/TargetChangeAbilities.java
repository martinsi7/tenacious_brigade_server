package activeAbilities;

import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.TargetInfo;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.AbilityTargetter;
import cardStatus.StatusChanger;

public class TargetChangeAbilities extends ActiveAbility {
	public AbilityTargetter targetter;
	public StatusChanger changer;
	public String attr;
	

	public TargetChangeAbilities(int newCost, CardInfo newOwner,
			int numOfTargets, AbilityTargetter newTargetter, String _attr,
			StatusChanger newChanger, boolean cooldownCost) {
		super(newCost, newOwner, numOfTargets, cooldownCost);
		targetter = newTargetter;
		changer = newChanger;
		attr = _attr;
	}
	
	public TargetChangeAbilities(int _cost, CardInfo _owner, int numTargets,
			AbilityTargetter _targetter, String _attr,
			boolean cooldownCost, boolean beginning, boolean end) {
		super(_cost, _owner, numTargets, cooldownCost);
		targetter = _targetter;
		attr = _attr;
		changer = new StatusChanger(_owner, beginning, end);
	}
	
	public TargetChangeAbilities(int _cost, CardInfo _owner, int numTargets,
			AbilityTargetter _targetter, String _attr, int amount,
			boolean cooldownCost, boolean beginning, boolean end) {
		super(_cost, _owner, numTargets, cooldownCost);
		targetter = _targetter;
		changer = new StatusChanger(_owner, beginning, end, amount);
		attr = _attr;
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		if (targetInfo.validTargets.size() > numTargets) {
			return;
		}
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : targetInfo.validTargets) {
			if (card instanceof UnitCard) {
				units.add((UnitCard) card);
			}
		}
		if (units.size() > 0 && canAfford()) {
			pay();
			for (UnitCard unit : units) {
				unit.attrs.addChanger(attr, changer);
			}
		}
		
	}

	@Override
	public TargetInfo getTargets() {
		TargetInfo targetInfo = new TargetInfo();
		for (UnitCard unit : targetter.getTargets(owner)) {
			targetInfo.validTargets.add(unit);
		}
		targetInfo.canTargetOtherPlayer = false;
		targetInfo.canTargetSelfPlayer = false;
		targetInfo.numTargets = numTargets;
		targetInfo.ownerCardID = owner.gameID;
		return targetInfo;
	}
}

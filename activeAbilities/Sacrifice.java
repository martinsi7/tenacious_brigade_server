package activeAbilities;

import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.TargetInfo;
import gameServer.UnitCard;

import java.util.ArrayList;
import java.util.HashMap;

import abilityTargetter.SameLaneTargetter;
import cardStatus.StatusChanger;

public class Sacrifice extends ActiveAbility {
	public HashMap<String, StatusChanger> changers;

	public Sacrifice(int newCost, CardInfo newOwner, int numOfTargets,
			boolean cooldownCost, HashMap<String, StatusChanger> _changers) {
		super(newCost, newOwner, numOfTargets, cooldownCost);
		changers = _changers;
	}
	
	public Sacrifice(int newCost, CardInfo newOwner, int numOfTargets,
			boolean cooldownCost, String[] attrs, int[] amounts, boolean _isBeginning, boolean _isEnd) {
		super(newCost, newOwner, numOfTargets, cooldownCost);
		changers = new HashMap<String, StatusChanger>();
		for (int i = 0; i < attrs.length; i++) {
			String attr = attrs[i];
			StatusChanger changer = null;
			if (i > amounts.length) {
				changer = new StatusChanger(owner, _isBeginning, _isEnd);
			} else {
				changer = new StatusChanger(owner, _isBeginning, _isEnd, amounts[i]);
			}
			changers.put(attr, changer);
		}
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		if (targetInfo.validTargets.size() > numTargets) {
			return;
		}
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : targetInfo.validTargets) {
			if (card instanceof UnitCard) {
				units.add((UnitCard) card);
			}
		}
		if (units.size() > 0 && canAfford()) {
			pay();
			for (UnitCard unit : units) {
				owner.goToClinic();
				owner.dirty = true;
				for (String attr : changers.keySet()) {
					unit.attrs.addChanger(attr, changers.get(attr));
					unit.dirty = true;
				}
			}
		}
	}

	@Override
	public TargetInfo getTargets() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		TargetInfo targetInfo = new TargetInfo();
		for (UnitCard unit : targetter.getTargets(owner)) {
			targetInfo.validTargets.add(unit);
		}
		targetInfo.canTargetOtherPlayer = false;
		targetInfo.canTargetSelfPlayer = false;
		targetInfo.numTargets = numTargets;
		targetInfo.ownerCardID = owner.gameID;
		return targetInfo;
	}

}

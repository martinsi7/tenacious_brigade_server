package activeAbilities;

import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.TargetInfo;
import gameServer.UnitCard;

import java.util.ArrayList;
import java.util.HashMap;

import abilityTargetter.AbilityTargetter;
import cardStatus.StatusChanger;

public class TargetAllLaneAbilities extends ActiveAbility {

	public AbilityTargetter targetter;
	public HashMap<String, StatusChanger> changers;
	
	public TargetAllLaneAbilities(int _cost, CardInfo _owner, int numTargets,
			AbilityTargetter _targetter, HashMap<String, StatusChanger> _changers, boolean cooldownCost) {
		super(_cost, _owner, numTargets, cooldownCost);
		targetter = _targetter;
		changers = _changers;
	}
	
	public TargetAllLaneAbilities(int _cost, CardInfo _owner, int numTargets,
			AbilityTargetter _targetter, boolean cooldownCost,
			String[] attrs, int[] amounts, boolean beginning, boolean end) {
		super(_cost, _owner, numTargets, cooldownCost);
		targetter = _targetter;
		changers = new HashMap<String, StatusChanger>();
		for (int i = 0; i < attrs.length; i++) {
			StatusChanger changer;
			if (i < amounts.length) {
				changer = new StatusChanger(_owner, beginning, end, amounts[i]);
			} else {
				changer = new StatusChanger(_owner, beginning, end);
			}
			changers.put(attrs[i], changer);
		}
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		if (targetInfo.validTargets.size() > numTargets) {
			return;
		}
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : targetInfo.validTargets) {
			if (card instanceof UnitCard) {
				units.add((UnitCard) card);
			}
		}
		if (units.size() > 0 && canAfford()) {
			pay();
			for (UnitCard target : units) {
				for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
					if (!(card instanceof UnitCard)) {
						continue;
					}
					UnitCard unit = (UnitCard) card;
					if (unit.zone == target.zone && unit.owner == target.owner) {
						for (String attr : changers.keySet()) {
							unit.attrs.addChanger(attr, changers.get(attr));
						}
					}
					
				}
			}
			
		}
		
	}

	@Override
	public TargetInfo getTargets() {
		TargetInfo targetInfo = new TargetInfo();
		for (UnitCard unit : targetter.getTargets(owner)) {
			targetInfo.validTargets.add(unit);
		}
		targetInfo.canTargetOtherPlayer = false;
		targetInfo.canTargetSelfPlayer = false;
		targetInfo.numTargets = numTargets;
		targetInfo.ownerCardID = owner.gameID;
		return targetInfo;
	}

}

package activeAbilities;

import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.Player;
import gameServer.TargetInfo;
import gameServer.UnitCard;

public class Infiltrate extends ActiveAbility {

	public Infiltrate(int newCost, CardInfo newOwner, int numOfTargets,
			boolean cooldownCost) {
		super(newCost, newOwner, numOfTargets, cooldownCost);
		description = "$" + cost ;
		if (cooldownCost) {
			description += ", cooldown";
		}
		description += ": Infiltrate";
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		int damage = 0;
		for (CardInfo card : owner.owner.gameState.cardsInPlay.values()) {
			if (card.owner != owner.owner && card.zone == owner.zone) {
				if (card instanceof UnitCard && ((UnitCard) card).isCooldown) {
					damage += ((UnitCard) card).cooldown;
				}
			}
		}
		if (damage > 0) {
			Player targetPlayer = null;
			if (owner.owner == owner.owner.gameState.curPlayer) {
				targetPlayer = owner.owner.gameState.otherPlayer;
			} else {
				targetPlayer = owner.owner.gameState.curPlayer;						
			}
			targetPlayer.health -= damage;
			targetPlayer.dirty = true;
		}
	}

	@Override
	public TargetInfo getTargets() {
		TargetInfo targetInfo = new TargetInfo();
		targetInfo.validTargets.add(owner);
		targetInfo.canTargetOtherPlayer = false;
		targetInfo.canTargetSelfPlayer = false;
		targetInfo.numTargets = 1;
		targetInfo.ownerCardID = owner.gameID;
		return targetInfo;
	}

}

package activeAbilities;

import abilityTargetter.AbilityTargetter;
import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.GameState;
import gameServer.TargetInfo;
import gameServer.UnitCard;

public class TargetChangeCooldown extends ActiveAbility {
	public int amount;
	public AbilityTargetter targetter;
	
	public TargetChangeCooldown(AbilityTargetter _targetter, int newCost, CardInfo owner,
			int newAmount, int numOfTargets, boolean cooldownCost) {
		super(newCost, owner, numOfTargets, cooldownCost);
		amount = newAmount;
		cost = newCost;
		description = "$" + cost + ": change target unit's cooldown by " + amount;
		numTargets = numOfTargets;
		targetter = _targetter;
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		if (canAfford()) {
			pay();
			int count = 0;
			for (CardInfo card : targetInfo.validTargets) {
				if (count > numTargets) {
					System.err.println("invalid number of Targets requested");
					break;
				}
				count++;
				if (!(card instanceof UnitCard)) {
					continue;
				}
				UnitCard unitCard = (UnitCard) card;
				unitCard.cooldown += amount;
				if (unitCard.cooldown < 0) {
					unitCard.cooldown = 0;
				}
				if (unitCard.cooldown == 0) {
					unitCard.isCooldown = false;
				} else {
					unitCard.isCooldown = true;
				}
				unitCard.dirty = true;
			}
		}

	}

	@Override
	public TargetInfo getTargets() {
		TargetInfo targets = new TargetInfo();
		GameState state = owner.owner.gameState;
		for (CardInfo card : state.cardsInPlay.values()) {
			if (!(card instanceof UnitCard)) {
				continue;
			}
			UnitCard unitCard = (UnitCard) card;
			if (unitCard.gameID != owner.gameID) {
				if (unitCard.isCooldown && amount < 0) {
					targets.validTargets.add(card);
				} else if (amount >= 0) {
					targets.validTargets.add(card);
				}
			}
		}
		targets.hasTarget = true;
		targets.ownerCardID = owner.gameID;
		targets.numTargets = numTargets;
		return targets;
	}

}

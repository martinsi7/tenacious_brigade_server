package activeAbilities;

import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.TargetInfo;
import gameServer.UnitCard;

import java.util.ArrayList;

import abilityTargetter.SameLaneTargetter;
import cardStatus.StatusChanger;

public class Cannibalize extends ActiveAbility {
	public int healthChange = 3;
	public int attackChange = 3;

	public Cannibalize(int newCost, CardInfo newOwner, int numOfTargets,
			boolean cooldownCost) {
		super(newCost, newOwner, numOfTargets, cooldownCost);
		// TODO Auto-generated constructor stub
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		if (targetInfo.validTargets.size() > numTargets) {
			return;
		}
		ArrayList<UnitCard> units = new ArrayList<UnitCard>();
		for (CardInfo card : targetInfo.validTargets) {
			if (card instanceof UnitCard) {
				units.add((UnitCard) card);
			}
		}
		if (units.size() > 0 && canAfford()) {
			pay();
			for (UnitCard unit : units) {
				owner.attrs.addChanger("health", new StatusChanger(owner, true, false, healthChange));
				owner.attrs.addChanger("attack", new StatusChanger(owner, true, false, attackChange));
				unit.goToClinic();
				unit.dirty = true;
			}
			owner.dirty = true;
		}
	}

	@Override
	public TargetInfo getTargets() {
		SameLaneTargetter targetter = new SameLaneTargetter();
		TargetInfo targetInfo = new TargetInfo();
		for (UnitCard unit : targetter.getTargets(owner)) {
			targetInfo.validTargets.add(unit);
		}
		targetInfo.canTargetOtherPlayer = false;
		targetInfo.canTargetSelfPlayer = false;
		targetInfo.numTargets = numTargets;
		targetInfo.ownerCardID = owner.gameID;
		return targetInfo;
	}

}

package activeAbilities;

import abilityTargetter.AbilityTargetter;
import gameServer.ActiveAbility;
import gameServer.CardInfo;
import gameServer.TargetInfo;
import gameServer.UnitCard;

public class CustomAbility extends ActiveAbility {
	private AbilityTargetter targetter;

	public CustomAbility(int newCost, CardInfo newOwner, int numOfTargets,
			boolean cooldownCost, AbilityTargetter _targetter) {
		super(newCost, newOwner, numOfTargets, cooldownCost);
		targetter = _targetter;
	}

	@Override
	public TargetInfo doInitAction() {
		return null;
	}

	@Override
	public TargetInfo doEventAction() {
		return getTargets();
	}

	@Override
	public void doResolvedAction(TargetInfo targetInfo) {
		if (targetInfo.validTargets.size() > numTargets || !canAfford()) {
			return;
		}
		pay();
		owner.customAbility(targetInfo);
	}

	@Override
	public TargetInfo getTargets() {
		TargetInfo targetInfo = new TargetInfo();
		for (UnitCard unit : targetter.getTargets(owner)) {
			targetInfo.validTargets.add(unit);
		}
		targetInfo.canTargetOtherPlayer = false;
		targetInfo.canTargetSelfPlayer = false;
		targetInfo.numTargets = numTargets;
		targetInfo.ownerCardID = owner.gameID;
		return targetInfo;
	}

}

package cardStatus;

import gameServer.CardInfo;

public class StatusChanger {
	public CardStatus owner;
	public CardInfo setter;
	public boolean isBeginning;
	public boolean isEnd;
	public int createdTurn;
	public int amount;
	public StatusChanger(CardInfo _setter,
			boolean _isBeginning, boolean _isEnd){
		setter = _setter;
		isBeginning = _isBeginning;
		isEnd = _isEnd;
		amount = 1;
	}
	public StatusChanger(CardInfo _setter,
			boolean _isBeginning, boolean _isEnd, int _amount){
		setter = _setter;
		isBeginning = _isBeginning;
		isEnd = _isEnd;
		amount = _amount;
	}
}

package cardStatus;

import gameServer.CardInfo;

import java.util.HashMap;
import java.util.Set;

public class AttributeHash {
	public CardInfo owner;
	
	public AttributeHash(CardInfo _owner) {
		attrs=new HashMap<String, CardStatus>();
		owner = _owner;
	}
	
	public int get(String attr) {
		attr = attr.toLowerCase();
		CardStatus ret = attrs.get(attr);
		if (ret == null) {
			return 0;
		} else {
			return ret.getValue();
		}
	}
	
	public boolean getBool(String attr) {
		attr = attr.toLowerCase();
		CardStatus ret = attrs.get(attr);
		if (ret == null) {
			return false;
		} else {
			return ret.getBoolValue();
		}
	}
	
	public int getOrig(String attr) {
		attr = attr.toLowerCase();
		CardStatus ret = attrs.get(attr);
		if (ret == null) {
			return 0;
		} else {
			return ret.getOrigValue();
		}
	}
	
	public boolean getOrigBool(String attr) {
		attr = attr.toLowerCase();
		CardStatus ret = attrs.get(attr);
		if (ret == null) {
			return false;
		} else {
			return ret.getOrigBoolValue();
		}
	}
	
	public void set(String key, CardStatus val) {
		key = key.toLowerCase();
		attrs.put(key, val);
	}
	
	public void set(String key, int val) {
		key = key.toLowerCase();
		CardStatus status = new CardStatus(int.class, owner);
		status.setValue(val);
		status.attrName = key;
		attrs.put(key, status);
	}
	
	public void set(String key, boolean val) {
		key = key.toLowerCase();
		CardStatus status = new CardStatus(boolean.class, owner);
		status.attrName = key;
		status.setValue(val);
		attrs.put(key, status);
	}
	
	public void change(String key, int amount) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		if (status == null) {
			return;
		}
		StatusChanger changer = new StatusChanger(null, false, false, amount);
		status.addChanger(changer);
	}
	
	public void setTrue(String key) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		if (status == null) {
			return;
		}
		status.setValue(true);
	}
	
	public void setFalse(String key) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		if (status == null) {
			return;
		}
		status.setValue(false);
	}
	
	public Set<String> getKeys() {
		return attrs.keySet();
	}
	
	public void updateStatus(String key) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		if (status != null) {
			status.updateStatus();
		}
	}
	
	public void updateStatus(String key, int turn, boolean end, boolean beginning) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		if (status != null) {
			status.updateStatus(turn, end, beginning);
		}
	}
	
	public boolean isBool(String key) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		if (status == null) {
			return false;
		}
		return status.isBoolValue;
	}
	
	public void addChanger(String key, StatusChanger changer) {
		key = key.toLowerCase();
		CardStatus status = attrs.get(key);
		status.addChanger(changer);
	}
	
	public void addChanger(String key, CardInfo setter, boolean beginning, boolean end, int amount) {
		key = key.toLowerCase();
		StatusChanger changer = new StatusChanger(setter, beginning, end, amount);
		CardStatus status = attrs.get(key);
		//if (status != null) {
		//	this.set(key, 0);
		//}
		status.addChanger(changer);
	}
	
	public void addChanger(String key, CardInfo setter, boolean beginning, boolean end) {
		key = key.toLowerCase();
		StatusChanger changer = new StatusChanger(setter, beginning, end);
		CardStatus status = attrs.get(key);
		//if (status != null) {
		//	this.set(key, false);
		//}
		status.addChanger(changer);
	}
	
	public boolean hasAttr(String attr) {
		return attrs.containsKey(attr);
	}
	
	private HashMap<String, CardStatus> attrs;

}

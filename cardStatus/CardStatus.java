package cardStatus;

import java.util.ArrayList;

import gameServer.CardInfo;

public class CardStatus {
	protected int value=0;
	protected int curValue=0;
	public boolean isBoolValue;
	public CardInfo owner;
	public String attrName;
	
	public CardStatus(Class<?> c, CardInfo _owner){
		if (c == int.class) {
			isBoolValue = false;
		} else {
			isBoolValue = true;
		}
		changers = new ArrayList<StatusChanger>();
		owner = _owner;
	}
	
	public CardStatus(Class<?> c, int newValue, CardInfo _owner){
		if (c == int.class) {
			isBoolValue = false;
		} else {
			isBoolValue = true;
		}
		changers = new ArrayList<StatusChanger>();
		setValue(newValue);
		owner = _owner;
	}
	
	public CardStatus(Class<?> c, boolean newValue, CardInfo _owner){
		if (c == int.class) {
			isBoolValue = false;
		} else {
			isBoolValue = true;
		}
		changers = new ArrayList<StatusChanger>();
		setValue(newValue);
		owner = _owner;
	}
	
	public int getOrigValue() {
		return value;
	}
	
	public boolean getOrigBoolValue() {
		return (value > 0);
	}
	
	public int getValue() {
		return curValue;
	}
	
	public boolean getBoolValue() {
		return (curValue > 0);
	}
	
	public void setValue(int newValue) {
		value = newValue;
		curValue = value;
	}
	
	public void setValue(boolean newValue) {
		value = (newValue) ? 1 : 0;
		curValue = value;
	}
	
	public void updateStatus(){
		updateStatus(-1, false, false);
	}
	
	public void updateStatus(int turn, boolean endOfTurn, boolean beginningOfTurn){
		boolean endDirty = false;
		boolean beginningDirty = false;
		curValue = value;
		for (int i = 0; i < changers.size(); i++) {
			StatusChanger changer = changers.get(i);
			if (endOfTurn && changer.createdTurn <= turn && changer.isEnd) {
				changers.remove(i);
				i--;
			} else if (beginningOfTurn && changer.createdTurn <= turn && changer.isBeginning) {
				changers.remove(i);
				i--;
			} else {
				if (changer.isEnd) {
					endDirty = true;
				} else if (changer.isBeginning) {
					beginningDirty = true;
				}
				updateValue(changer.amount);
			}
		}
		if (endDirty) {
			owner.endDirty = true;
		}
		if (beginningDirty) {
			owner.beginningDirty = true;
		}
		owner.dirty = true;
	}
	
	private void updateValue(int addedValue) {
		if (isBoolValue) {
			curValue = addedValue;
		} else {
			curValue += addedValue;
		}
		owner.dirty = true;
	}
	
	public void addChanger(StatusChanger changer){
		changer.owner = this;
		changer.createdTurn = owner.owner.gameState.turn;
		if (changer.isEnd) {
			owner.endDirty = true;
		} else if (changer.isBeginning) {
			owner.beginningDirty = true;
		}
		changers.add(changer);
		updateStatus();
	}
	
	
	protected ArrayList<StatusChanger> changers;
}
